$(function () {
    var address = '';
    $('#province').bind('change', function () {
        address = '';
        var province_id = $('#province option:selected').val();
        address = $('#province option:selected').text();
        $('#address').val(address);
        $("#city").empty();
        $("#city").append('<option value="0">城市</option>');
        for (var i in citylist[province_id]['city']) {
            if (citylist[province_id]['city'][i]['id'] == city_id) {
                $("#city").append("<option selected value='" + citylist[province_id]['city'][i]['id'] + "'>" + citylist[province_id]['city'][i]['name'] + "</option>");
            } else {
                $("#city").append("<option value='" + citylist[province_id]['city'][i]['id'] + "'>" + citylist[province_id]['city'][i]['name'] + "</option>");
            }
        }
    });
    $('#city').bind('change', function () {
        var province_id = $('#province option:selected').val();
        var city_id = $('#city option:selected').val();
        address = $('#province option:selected').text() + $('#city option:selected').text();
        $('#address').val(address);
        $("#area").empty();
        $("#area").append('<option value="0">区</option>');
        for (var i in citylist[province_id]['city'][city_id]['area']) {
            if (citylist[province_id]['city'][city_id]['area'][i]['id'] == area_id) {
                $("#area").append("<option selected value='" + citylist[province_id]['city'][city_id]['area'][i]['id'] + "'>" + citylist[province_id]['city'][city_id]['area'][i]['name'] + "</option>");
            } else {
                $("#area").append("<option  value='" + citylist[province_id]['city'][city_id]['area'][i]['id'] + "'>" + citylist[province_id]['city'][city_id]['area'][i]['name'] + "</option>");
            }
        }
    });
    $('#area').change(function () {
        address = $('#province option:selected').text() + $('#city option:selected').text() + $('#area option:selected').text();
        $('#address').val(address);
    })
    for (var i in citylist) {
        if (citylist[i]['id'] == province_id) {
            $("#province").append("<option selected value='" + citylist[i]['id'] + "'>" + citylist[i]['name'] + "</option>");
        } else {
            $("#province").append("<option value='" + citylist[i]['id'] + "'>" + citylist[i]['name'] + "</option>");
        }
    }
    $("#province").trigger("change");
    $('#city').trigger("change");
})