$(function () {
    var address = '';
    for (var i in citylist) {
        $("#province").append("<option value='" + citylist[i]['id'] + "'>" + citylist[i]['name'] + "</option>");
    }
    $('#province').change(function () {
        address = '';
        var province_id = $('#province option:selected').val();
        address = $('#province option:selected').text();
        $('#address').val(address);
        $("#city").empty();
        $("#city").append('<option value="0">城市</option>');
        for (var i in citylist[province_id]['city']) {
            $("#city").append("<option value='" + citylist[province_id]['city'][i]['id'] + "'>" + citylist[province_id]['city'][i]['name'] + "</option>");
        }
    });
    $('#city').change(function () {
        var province_id = $('#province option:selected').val();
        var city_id = $('#city option:selected').val();
        address = $('#province option:selected').text() + $('#city option:selected').text();
        $('#address').val(address);
        $("#area").empty();
        $("#area").append('<option value="0">区</option>');
        for (var i in citylist[province_id]['city'][city_id]['area']) {
            $("#area").append("<option value='" + citylist[province_id]['city'][city_id]['area'][i]['id'] + "'>" + citylist[province_id]['city'][city_id]['area'][i]['name'] + "</option>");
        }
    });
    $('#area').change(function () {
        address = $('#province option:selected').text() + $('#city option:selected').text() + $('#area option:selected').text();
        $('#address').val(address);
    })
})