<?php
/**
 * 配置文件
 */

return [
    // 数据库类型
    'type'     => 'mysql',
    'hostname' => isset($_SERVER['DB_HOST']) ? $_SERVER['DB_HOST'] : '',
    // 数据库名
    'database' => isset($_SERVER['DB_NAME']) ? $_SERVER['DB_NAME'] : '',
    // 用户名
    'username' => isset($_SERVER['DB_USERNAME']) ? $_SERVER['DB_USERNAME'] : '',
    // 密码
    'password' => isset($_SERVER['DB_PWD']) ? $_SERVER['DB_PWD'] : '',
    // 端口
    'hostport' => isset($_SERVER['DB_PORT']) ? $_SERVER['DB_PORT'] : '3306',
    // 数据库编码默认采用utf8
    'charset'  => 'utf8mb4',
    // 数据库表前缀
    'prefix'   => 'cmf_',
    "authcode" => 'v7YfOC7aa1mPV7pgoC',
    //#COOKIE_PREFIX#
];
