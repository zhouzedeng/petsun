<?php

use think\exception\HttpException;

/**
 * @param string $msg
 * @param array $data
 * @return \think\response\Json
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function apiSuccess($data = [], $msg = '请求成功')
{
    empty($data) && $data = new \stdClass();
    $result = [
        'code' => 0,
        'msg' => $msg,
        'data' => $data
    ];
    $response = \think\Response::create($result, 'json', 200);
    throw new \think\exception\HttpResponseException($response);

}

/**
 * @param int $code
 * @param string $msg
 * @throws HttpException
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function apiLog($param, $return)
{
    $log = [
        '请求路径' => request()->controller() . '/' . request()->method(),
        "请求参数" => $param,
        '返回' => $return
    ];
    \think\Log::error($log);
}

/**
 * @param int $code
 * @param string $msg
 * @throws HttpException
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function apiFail($code = -1, $msg = '请求失败')
{
    apiLog($msg, '');
    throw new HttpException($code, $msg);
}

/**
 * @param $result
 * @return \think\response\Json
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function jsonReturn($result)
{
    $origin = '*';
    $header = ['Access-Control-Allow-Headers' => 'x-requested-with,content-type', 'Access-Control-Allow-Origin' => $origin];
    return json($result, 200, $header);
}

/**
 * @return array
 * @Author Zed
 * @Time 2019/3/12 11:02
 */
function getCouponList()
{
    $coupon = [
        ['BuyNumber' => 1, 'GiveNumber' => 1, 'type' => 1, 'Limit' => 3, 'desc' => '本次优惠以医院为单位，上限是三，之后为买二赠一'],
        ['BuyNumber' => 2, 'GiveNumber' => 1, 'type' => 2, 'Limit' => 0, 'desc' => '本次优惠买二赠一，数量无上限']

    ];
    return $coupon;
}

/**
 * @return false|string
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function getNow()
{
    return Date("Y-m-d H:i:s", time());
}

/**
 * @return false|string
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function getToken()
{
    return md5(uniqid() . uniqid() . uniqid());
}


/**
 * @return false|string
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function getPetSunUserPrefixKey()
{
    return 'petsun_user:';
}


/**
 * @return false|string
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function obj2arr($obj)
{
    return json_decode(json_encode($obj), true);
}

/**
 * @return false|string
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function getPositionById($id = 0)
{
    if($id===null){
        return '';
    }
    $arr = array('助理', '医生', '店长');
    return $arr[$id];
}

/**
 * @return false|string
 * @Author Zed
 * @Time 2019/2/28 14:33
 */
function getTsList()
{
    return [
        ['id' => 1, 'name' => '胃镜'],
        ['id' => 2, 'name' => '监护仪'],
        ['id' => 3, 'name' => 'CT'],
        ['id' => 4, 'name' => 'MR'],
        ['id' => 5, 'name' => 'DR'],
        ['id' => 6, 'name' => 'B超'],
        ['id' => 7, 'name' => '生化分析仪'],
        ['id' => 8, 'name' => '血液细胞分析仪'],
    ];
}

/**
 * 变量友好化打印输出
 * @example dump($a,$b,$c,$e,[.1]) 支持多变量，使用英文逗号符号分隔，默认方式 print_r，查看数据类型传入 .1
 * @version php4、5、7
 * @return void
 */
function p()
{
    header("Content-Type:text/html;charset=UTF-8");
    echo '<style>.php-print{background:#eee;padding:10px;border-radius:4px;border:1px solid #ccc;line-height:1.5;white-space:pre-wrap;font-family:Menlo,Monaco,Consolas,"Courier New",monospace;font-size:13px;}</style>', '<pre class="php-print">';

    $args = func_get_args();
    if (end($args) === .1) {
        array_splice($args, -1, 1);

        foreach ($args as $k => $v) {
            echo $k > 0 ? '<hr>' : '';

            ob_start();
            var_dump($v);

            echo preg_replace('/]=>\s+/', '] => <label>', ob_get_clean());
        }
    } else {
        foreach ($args as $k => $v) {
            echo $k > 0 ? '<hr>' : '', print_r($v, true);
        }
    }
    echo '</pre>';
    die;
}

function getByCurl($url = '')
{
    //初始化
    $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    if (!empty($header)) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    }
    if (strpos($url, 'https:') !== FALSE) {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    }
    //执行命令
    $output = curl_exec($curl);
    //关闭URL请求
    curl_close($curl);
    //显示获得的数据
    return $output;
}


/**
 * Gets the value of an environment variable.
 *
 * @param  string $key
 * @param  mixed $default
 *
 * @return mixed
 */
function env($key, $default = null)
{
    $value = getenv($key);

    if ($value === false) {
        return value($default);
    }

    if (envSubstr($value)) {
        return substr($value, 1, -1);
    }

    return envConversion($value);
}

function value($value)
{
    return $value instanceof Closure ? $value() : $value;
}

function envSubstr($value)
{
    return ($valueLength = strlen($value)) > 1 && strpos($value, '"') === 0 && $value[$valueLength - 1] === '"';
}

/**
 * @param $value
 *
 * @return bool|string|null
 */
function envConversion($value)
{
    $key = strtolower($value);

    if ($key === 'null' || $key === '(null)') {
        return null;
    }

    $list = [
        'true' => true,
        '(true)' => true,
        'false' => false,
        '(false)' => false,
        'empty' => '',
        '(empty)' => '',
    ];

    return isset($list[$key]) ? $list[$key] : $value;
}

function envNotEmpty($key)
{
    $value = env($key, false);
    if ($value !== false && !$value) {
        throw new \AlibabaCloud\Client\Exception\ClientException(
            "Environment variable '$key' cannot be empty",
            \AlibabaCloud\Client\SDK::INVALID_ARGUMENT
        );
    }
    if ($value) {
        return $value;
    }

    return false;
}

/**
 * @return false|string
 * @Author quan
 * @Time 2019/3/16 14:33
 */
function getScanStatusById($id = 0)
{
    $arr = array(
        0 => '待扫描', 1 => '已扫描');
    return $arr[$id];
}

function isMobile($text)
{
    $search = '/^0?1[3|4|5|6|7|8|9][0-9]\d{8}$/';
    if (preg_match($search, $text)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 对象 转 数组
 *
 * @param object $obj 对象
 * @return array
 */
function object_to_array($obj) {
    $obj = (array)$obj;
    foreach ($obj as $k => $v) {
        if (gettype($v) == 'resource') {
            return;
        }
        if (gettype($v) == 'object' || gettype($v) == 'array') {
            $obj[$k] = (array)object_to_array($v);
        }
    }

    return $obj;
}

/*
* 中国正常GCJ02坐标---->百度地图BD09坐标
* 腾讯地图用的也是GCJ02坐标
* @param double $lat 纬度
* @param double $lng 经度
*/

function tx_to_bd($lat,$lng){
    $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    $x = $lng;
    $y = $lat;
    $z =sqrt($x * $x + $y * $y) + 0.00002 * sin($y * $x_pi);
    $theta = atan2($y, $x) + 0.000003 * cos($x * $x_pi);
    $lng = $z * cos($theta) + 0.0065;
    $lat = $z * sin($theta) + 0.006;
    return array('lng'=>$lng,'lat'=>$lat);
}


/*
* 百度地图BD09坐标---->中国正常GCJ02坐标
* 腾讯地图用的也是GCJ02坐标
* @param double $lat 纬度
* @param double $lng 经度
* @return array();
*/

function bd_to_tx($lat,$lng){
    $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    $x = $lng - 0.0065;
    $y = $lat - 0.006;
    $z = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $x_pi);
    $theta = atan2($y, $x) - 0.000003 * cos($x * $x_pi);
    $lng = $z * cos($theta);
    $lat = $z * sin($theta);
    return array('lng'=>$lng,'lat'=>$lat);
}