<?php

namespace app\comm\model;

use think\exception\HttpException;
use think\Log;
use think\Model;

class BaseModel extends Model
{
    protected $table = '';
    protected $id = "id";
    protected $fields = '*';

    /**
     * 获取列表数据
     * @param array $where 条件参数
     * @param string|array $field 需返回的字段
     * @param int $page 当前页
     * @param int $pagesize 每页条数
     * @param string|array $order 排序
     * @throws \think\exception
     * @return array
     * @author Zed
     * @since 2018-7-24
     */
    public function getListByParam($where = array(), $page = null, $pagesize = null, $field = null, $order = null)
    {
        $query = db()->table($this->getTable());
        // 条件
        if ($where) {
            $query->where($where);
        }
        // 如传入的$field为空是真值，则$field使用在model中定义的field。若model中没有定义field属性，则返回全部字段
        if (empty($field)) {
            $field = $this->getMyField();
        }
        // 排序
        if ($order) {
            $query->order($order);
        }
        // 分页限制
        if ($page) {
            $query->page($page, $pagesize);
        }
        $result = $query->field($field)->select();
        if ($result === false) {
            Log::error('获取列表失败');
            throw new HttpException(Code::MYSQL_QUERY_FAIL, lang('operation fail'));
        }
        return $result;
    }

    /**
     * @param array $where
     * @return int|string
     * @throws \think\Exception
     * @Author Zed
     * @Time 2019/2/28 11:33
     */
    public function getCntByParam($where = array())
    {
        $query = db()->table($this->getTable());
        // 条件
        if ($where) {
            $query->where($where);
        }
        $result = $query->count();
        if ($result === false) {
            Log::error('获取总数失败');
            throw new HttpException(Code::MYSQL_QUERY_FAIL, lang('operation fail'));
        }
        return $result;
    }

    /**
     * 查询一条记录
     * @param $where
     * @param string|array $field 需返回的字段
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\exception
     * @author Zed
     * @since 2018-7-24
     */
    public function getOneByParam($where = array(), $field = null)
    {
        $query = db()->table($this->getTable());
        // 如传入的$field为空是真值，则$field使用在model中定义的field。若model中没有定义field属性，则返回全部字段
        if (empty($field)) {
            $field = $this->getMyField();
        }
        if ($where) {
            $query->where($where);
        }
        $result = $query->field($field)->find();
        if ($result === false) {
            Log::error('数据库执行失败，获取数据失败');
            throw new HttpException(Code::MYSQL_QUERY_FAIL, lang('operation fail'));
        }
        return $result;
    }

    /**
     * @param $data
     * @param $id
     * @return int
     * @Author Zed
     * @Time 2019/2/28 11:33
     */
    public function updateById($data, $id)
    {
        // 定义变量
        $cnt = 0;
        $id_list = is_array($id) ? $id : [$id];
        $id_list = array_chunk($id_list, 1000); // 避免使用 in 时数据超过1000条导致数据库性能下降

        // 循环更新子列表
        foreach ($id_list as $v) {
            $map = [];
            $map[$this->getMyId()] = array('in', $v);
            $query = db()->table($this->getTable())->where($map);
            $result = $query->update($data);
            $cnt += $result;
        }
        return $cnt;
    }

    /**
     * 删除记录（列表），根据ID
     * @param $id
     * @return int
     * @throws \think\exception
     * @author Zed
     * @since 2018-7-19
     */
    public function delById($id)
    {
        // 定义变量
        $cnt = 0;
        $id_list = is_array($id) ? $id : array($id);
        $id_list = array_chunk($id_list, 1000);

        // 循环删除子列表
        foreach ($id_list as $v) {
            $map = array();
            $map[$this->getMyId()] = array('in', $v);
            $query = db()->table($this->getTable())->where($map);
            $result = $query->delete();
            $cnt += $result;
        }
        return $cnt;
    }

    /**
     * 新增记录
     * @param array $data
     * @return int 新增记录行的id
     * @author Zed
     * @since 2018-7-19
     */
    public function add($data)
    {
        $id = db()->table($this->getTable())->insertGetId($data);
        if (empty($id)) {
            Log::error('数据库新增失败');
            throw new HttpException(Code::MYSQL_QUERY_FAIL, lang('operation fail'));
        }
        return $id;
    }

    /**
     * 批量新增
     * @param array $data
     * @return int  插入的行数
     * @author Zed
     * @since 2018-7-19
     */
    public function addList($data)
    {
        // 定义变量
        $cnt = 0;
        $list = array_chunk($data, 1000);
        // 循环插入子列表
        foreach ($list as $v) {
            $result = db()->table($this->getTable())->insertAll($v);
            if (!$result) {
                return 0;
            }
            $cnt += $result;
        }
        return $cnt;
    }

    /**
     * 获取表自定义主键
     * @return string
     * @author Zed
     * @since 2018-7-19
     */
    private function getMyId()
    {
        $full_class_name = get_called_class();
        $class = new $full_class_name;
        return isset($class->id) ? $class->id : $this->id;
    }

    /**
     * 获取表自定义返回字段域
     * @return string
     * @author Zed
     * @since 2018-7-19
     */
    private function getMyField()
    {
        $full_class_name = get_called_class();
        $class = new $full_class_name;
        return isset($class->fields) ? $class->fields : $this->fields;
    }

    /**
     *  上传图片表
     */
    public function upImg($imgs_name, $id = '')
    {
        $imgs_model = new ImgsModel();

        if (!empty($id)) {
            $imgs_model->save(['img_name'=>$imgs_name],['id'=>$id]);
            return $id;
        }
        if (empty($imgs_name)) {
            return 0;
        }
        $data = ['img_name' => $imgs_name, 'created_at' => time()];
        $id = $imgs_model->add($data);
        return $id;
    }
}