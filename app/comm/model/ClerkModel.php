<?php

namespace app\comm\model;

class ClerkModel extends BaseModel
{
    protected $table = "cmf_petsun_clerk"; // 表名
    protected $id = "id"; // 主键

    /**
     * @param int $id
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/18 15:08
     */
    public function getClerkById($id = 0)
    {
        $param = [];
        $param['id'] = $id;
        $clerk = $this->getOneByParam($param);
        return $clerk;
    }
}