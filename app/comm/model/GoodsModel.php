<?php
namespace app\comm\model;

class GoodsModel extends BaseModel
{
    protected $table = "cmf_petsun_goods"; // 表名
    protected $id = "id"; // 主键

    /**
     * @param $userId
     * @param $storeId
     * @param $agenterId
     * @param $companyId
     * @param string $address
     * @param $goodsIds
     * @return mixed
     * @Author Zed
     * @Time 2019/4/19 15:09
     */
    public function updateGoods($userId, $storeId, $agenterId, $companyId, $address = '', $goodsIds)
    {
        $editData = [
            'scan_status' => 1,
            'scan_time' => time(),
            'scan_address' => $address,
            'clerk_id' => $userId,
            'store_id' => $storeId,
            'agenter_id' => $agenterId,
            'company_id' => $companyId,
            'created_at' => time(),
        ];
        $res = $this->updateById($editData, $goodsIds);
        return $res;
    }

}