<?php
namespace app\comm\lib;

use think\exception\Handle;
use think\exception\HttpException;

class Http extends Handle
{
    public function render(\Exception $e)
    {
        $statusCode = -1;
        if ($e instanceof HttpException) {
            $statusCode = $e->getStatusCode();
        }

        $result = [
                'code'=> $statusCode,
                'msg'=>(string)$e->getMessage(),
        ];
        return json($result);
    }
}