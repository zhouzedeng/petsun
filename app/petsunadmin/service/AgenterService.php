<?php

namespace app\petsunadmin\service;


use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\StoreModel;

class AgenterService
{
    /**
     * 代理管理列表
     */
    public function getList($param)
    {
        $agenter_model = new AgenterModel();
        $agenter_model->alias('a');
        $agenter_model->field('a.*,IFNULL(sum(gs.sale_num),0) as num');
        $agenter_model->join('cmf_petsun_agenter_goods_sale gs', 'gs.agenter_id=a.id', 'left');
        $where = ['a.company_id' => $param['pid'], 'a.delete_time' => 0];
        $buildSql = $agenter_model->where($where)->order(['a.id' => 'ASC'])->group('a.id')->buildSql();
        $data = $agenter_model->table($buildSql . ' a')
            ->field('a.*,(IFNULL(sum(gs.sale_num),0)+a.num) num')
            ->join('cmf_petsun_store s', 's.agenter_id=a.id and s.delete_time=0', 'left')
            ->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id ', 'left')
            ->group('a.id')->paginate(10);
        return $data;
    }

    /*
     * 添加代理
     */
    public function add($data)
    {
        $agenter_model = new AgenterModel();

        if (session('user_type') != 3) {
            return '您不是代理公司管理帐号';
        }
        $clerk_model = new ClerkModel();
        $clerk_res = $clerk_model->where(['phone' => $data['phone'], 'delete_time' => 0])->find();
        if ($clerk_res) {
            return '该手机号已占用';
        }
        $pid = session('pid');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['company_id'] = $pid;
        $res = $agenter_model->insert($data);
        return $res;
    }


}