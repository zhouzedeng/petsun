<?php
namespace app\petsunadmin\service;

use app\comm\model\GoodsModel;
use app\comm\model\QrcodeModel;

class SuperviseService
{
    /**
     * 获取监管码列表
     */
    public function getList($param)
    {
        $goods_model = new GoodsModel();
        $goods_model->alias('g');
        if (isset($param['goods_serial']) && !empty($param['goods_serial'])) {
            $goods_model->where('goods_serial', 'like', '%' . $param['goods_serial'] . '%');
        }
        $goods_model->join('cmf_petsun_imgs pi', 'pi.id=g.qrcode_img','left');
        $goods_model->join('cmf_petsun_company pc', 'pc.id=g.company_id','left');
        $goods_model->join('cmf_petsun_store ps', 'ps.id=g.store_id','left');
        $goods_model->join('cmf_petsun_agenter pa', 'pa.id=g.agenter_id','left');
        $goods_model->join('cmf_petsun_clerk pk', 'pk.id=g.clerk_id','left');
        $list = $goods_model->field('g.*,pi.img_name as img_name,pa.name as agenter_name,pa.phone as agenter_phone,pc.company_name,ps.store_name,pk.position,pk.name as clerk_name,pk.phone as clerk_phone')
            ->order( ['g.id' => 'ASC'])
            ->paginate(10);
        $list->appends($param);
        return $list;
    }
}