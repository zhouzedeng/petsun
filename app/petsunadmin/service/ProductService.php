<?php

namespace app\petsunadmin\service;

use app\comm\model\ProductModel;

class ProductService
{
    /**
     * 获取产品列表
     */
    public function getProductList($param)
    {
        $product_model = new ProductModel();
        $product_model->alias('p');
        $where = ['p.delete_time' => 0];
        if (isset($param['name']) && !empty($param['name'])) {
            $product_model->where('p.name', 'like', '%' . $param['name'] . '%');
        }
        $product_model->join('cmf_petsun_imgs pi', 'pi.id=p.img_url','left');
        $product_model->where($where);
        $list = $product_model->field('p.*,pi.img_name as img_url')
            ->order(['p.id' => 'ASC'])
            ->paginate(10) ;
        $list->appends($param);
        return $list;
    }

    /**
     * 添加产品
     * @param array $data 页面数据
     * @return $this
     */
    public function add($data)
    {
        $product_model = new ProductModel();

        $data ['created_at'] = date('Y-m-d H:i:s');
        $imags = [];
        foreach ($data['img_url'] as $k => $v) {
            if (!empty($v)) {
                $imags[] = $product_model->upImg($v);
            }
        }
        $data['img_url'] = implode(',', $imags);;
        $res = $product_model->allowField(true)->data($data, true)->save();
        return $res;

    }

}