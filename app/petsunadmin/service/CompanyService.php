<?php

namespace app\petsunadmin\service;

use app\comm\model\AdModel;
use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\CompanyModel;
use app\comm\model\StoreGoodsSaleModel;
use app\comm\model\StoreModel;
use app\user\model\UserModel;
use think\Db;

class CompanyService
{
    /**
     * 获取代理商公司列表
     */
    public function getList($param, $limit = 10)
    {
        $where = ['c.delete_time' => 0];

        $company_model = new CompanyModel();
        $company_model->alias('c');
        $company_model->field('c.*,IFNULL(sum(gs.sale_num),0) num')
            ->order(['id' => 'AEC']);
        if (isset($param['company_name']) && !empty($param['company_name'])) {
            $company_model->where('company_name', 'like', '%' . $param['company_name'] . '%');
        }
        if (isset($param['ceo_name']) && !empty($param['ceo_name'])) {
            $company_model->where('ceo_name', 'like', '%' . $param['ceo_name'] . '%');
        }
        if (isset($param['ceo_phone']) && !empty($param['ceo_phone'])) {
            $company_model->where('ceo_phone', 'like', '%' . $param['ceo_phone'] . '%');
        }
        $company_model->join('cmf_petsun_store s', 'c.id=s.company_id', 'left');
        $company_model->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id', 'left');
        $company_model->group('c.id');
        $company_model->where($where);
        $company_buildSql = $company_model->buildSql();
        $list = $company_model->table($company_buildSql . ' a')
            #  ->field('a.*,(IFNULL(sum(ags.sale_num),0)+a.num) num,(IFNULL(sum(IFNULL(pp.price,0)*ags.sale_num),0)+a.total_price) total_price,(IFNULL(sum(ags.give_num),0)+a.song_num) song_num,pp.name')
            ->field('a.*,(IFNULL(sum(ags.sale_num),0)+a.num) num')
            ->join('cmf_petsun_agenter pa', 'pa.company_id=a.id', 'left')
            ->join('cmf_petsun_agenter_goods_sale ags', 'ags.agenter_id=pa.id', 'left')
            # ->join('cmf_petsun_product pp', 'pp.name=ags.goods_name', 'left')
            ->group('a.id')->paginate($limit);
        $list->appends($param);
        return $list;
    }

    /**
     * 添加代理商公司
     * @param array $data 页面数据
     * @return $this
     */
    public function addCompany($data)
    {
        $company_model = new CompanyModel();
        $user_model = Db::name('user');
        $company_model->startTrans();
        $user_info = $user_model->where(['user_login' => $data['ceo_phone']])->find();
        if ($user_info) {
            return '手机号已经绑定其他代理商';
        }
        $data ['created_at'] = date('Y-m-d H:i:s');
        $company_id = $company_model->insertGetId($data);

        $user ['user_pass'] = cmf_password(substr($data['ceo_phone'], -6));
        $user ['create_time'] = time();
        $user ['pid'] = $company_id;
        $user ['user_type'] = 3;
        $user ['user_login'] = $data['ceo_phone'];
        $user_id = $user_model->insertGetId($user);
        if ($company_id && $user_id) {
            //代理默认角色是2
            Db::name('RoleUser')->insert(["role_id" => 2, "user_id" => $user_id]);
            $company_model->commit();
            return true;
        } else {
            $company_model->rollback();
            return '添加失败';
        }
    }

    /**
     * 删除代理商详情
     */
    public function deleteCompany($data)
    {
        $company_model = new CompanyModel();
        $updata = ['delete_time' => time()];
        $res = $company_model->save($updata, ['id' => $data['id']]);
        return $res;
    }

    /**
     * 代理商公司详情
     */
    public function details($data)
    {
        $company_model = new CompanyModel();
        $info = $company_model->where(['id' => $data['id']])->find();
        $agenter_model = new AgenterModel();
        $agenter_model->alias('a');
        $agenter_model->field('a.*,IFNULL(sum(gs.sale_num),0) as num');
        $agenter_model->join('cmf_petsun_agenter_goods_sale gs', 'gs.agenter_id=a.id', 'left');
        $agenter_buildSql = $agenter_model->where(['a.company_id' => $info['id'], 'a.delete_time' => 0])
            ->group('a.id')->buildSql();
        $agenter_list = $agenter_model->table($agenter_buildSql . ' a')
            ->field('a.*,(IFNULL(sum(gs.sale_num),0)+a.num) num')
            ->join('cmf_petsun_store s', 's.agenter_id=a.id and s.delete_time=0', 'left')
            ->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id ', 'left')
            ->group('a.id')->paginate(10);
        $agenter_list->appends($data);
        $datails ['info'] = $info;
        $datails ['agenter_list'] = $agenter_list;
        return $datails;
    }

    /**
     * 代理人详情
     */
    public function agenterDetails($data)
    {
        $agenter_model = new AgenterModel();
        $agenter_info = $agenter_model->where(['id' => $data['id']])->find();
        $company_model = new CompanyModel();
        $company_info = $company_model->where(['id' => $agenter_info['company_id']])->find();
        $store_model = new StoreModel();
        $store_buildSql = $store_model
            ->alias('s')
            ->where(['s.agenter_id' => $agenter_info['id'], 's.delete_time' => 0])
            ->join('cmf_petsun_clerk pc', 'pc.id=s.store_owner_id', 'left')
            ->join('cmf_petsun_clerk pca', 'pca.store_id=s.id', 'left')
            #->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id', 'left')
            #->field('s.*,pc.name as clerk_name,pc.phone as clerk_phone,COUNT(pca.id) AS doctor_num,IFNULL(sum(gs.sale_num),0) as buy_num,IFNULL(sum(gs.song_num),0) as give_num')
            ->field('s.*,pc.name as clerk_name,pc.phone as clerk_phone,COUNT(pca.id) AS doctor_num')
            ->group('s.id')
            ->buildSql();
        $store_goods_sale_model = new StoreGoodsSaleModel();
        $store_list = $store_goods_sale_model->table($store_buildSql . ' a')
            ->field('a.*')
            #->field('a.*,IFNULL(sum(gs.sale_num),0) as buy_num,IFNULL(sum(gs.song_num),0) as give_num')
            ->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=a.id', 'left')
            ->group('a.id')->paginate(10);
        $store_list->appends($data);
        /*       $arr_list = $store_list->toArray()['data'];
               $a = array_column($arr_list,'id');*/
        $agenter_info ['company_name'] = $company_info['company_name'];
        $datails ['info'] = $agenter_info;
        $datails ['store_list'] = $store_list;
        return $datails;
    }

    /**
     * 获取人员列表
     */
    public function clerkList($data = [])
    {
        $clerkModel = new ClerkModel();
        $storeGoodsSalesModel = new StoreGoodsSaleModel();

        $param = [];
        $param['delete_time'] = 0;
        $param['store_id'] = $data['id'];
        $list = $clerkModel->getListByParam($param, null, null, null, ['position' => 'desc']);
        $list = obj2arr($list);
        $clerk_ids = array_column($list, 'id');

        $param = [];
        $param['clerk_id'] = ['in', $clerk_ids];
        $sale_list = $storeGoodsSalesModel->getListByParam($param);
        $sale_list = obj2arr($sale_list);

        $tmp = [];
        foreach ($sale_list as $v) {
            if (isset($tmp[$v['clerk_id']])) {
                $tmp[$v['clerk_id']]['song_num'] += $v['song_num'];
                $tmp[$v['clerk_id']]['num'] += $v['sale_num'];
            } else {
                $tmp[$v['clerk_id']]['song_num'] = $v['song_num'];
                $tmp[$v['clerk_id']]['num'] = $v['sale_num'];
            }
        }

        foreach ($list as $k => $v) {
            $list[$k]['num'] = isset($tmp[$v['id']]) ? $tmp[$v['id']]['num'] : 0;
            $list[$k]['song_num'] = isset($tmp[$v['id']]) ? $tmp[$v['id']]['song_num'] : 0;
        }

        return $list;
    }

    /**
     * 获取代理商列表
     */
    public function getCompanyList()
    {

        $model = new CompanyModel();
        $list = $model->where(['delete_time' => 0])->select();
        return $list;
    }
}