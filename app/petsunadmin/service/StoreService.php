<?php

namespace app\petsunadmin\service;


use app\comm\model\AdModel;
use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\ImgsModel;
use app\comm\model\StoreGoodsSaleModel;
use app\comm\model\StoreModel;
use think\Debug;

class StoreService
{
    /**
     * 获取医院列表
     */
    public function getStoreList($param)
    {
        $store_model = new StoreModel();
        $clerkModel = new ClerkModel();
        $store_model->alias('s');
        $where = ['s.delete_time' => 0, 's.check_status' => 1];
        if (session('user_type') == 3) {
            $where ['s.company_id'] = session('pid');
        }
        # $store_model->field('s.*,group_concat(pi.img_name) as img_name,pc.company_name,pa.name as agenter_name,pa.phone as agenter_phone,concat(pr.name,pra.name,prb.name) address');
        $store_model->field('s.*,pc.company_name,pa.name as agenter_name,pa.phone as agenter_phone,concat(pr.name,pra.name,prb.name) address');
        if (isset($param['store_name']) && !empty($param['store_name'])) {
            $store_model->where('store_name', 'like', '%' . $param['store_name'] . '%');
        }
        $store_model->join('cmf_petsun_company pc', 'pc.id=s.company_id', 'left');
        $store_model->join('cmf_petsun_agenter pa', 'pa.id=s.agenter_id and pa.delete_time=0', 'left');
        #$store_model->join('cmf_petsun_imgs pi', 'FIND_IN_SET(pi.id, s.store_imgs)', 'left');
        $store_model->join('cmf_petsun_region pr', 'pr.id=s.province_id', 'left');
        $store_model->join('cmf_petsun_region pra', 'pra.id=s.city_id', 'left');
        $store_model->join('cmf_petsun_region prb', 'prb.id=s.area_id', 'left');
        $store_model->where($where);
        $store_model->group('s.id');
        $list = $store_model->order(['s.id' => 'desc'])->paginate(10);

        /*    $store_model->alias('s');
            $where = ['s.delete_time' => 0, 's.check_status' => 1];
            if (session('user_type') == 3) {
                $where ['s.company_id'] = session('pid');
            }
            $store_model->field('s.*,group_c o ncat(pi.img_name) as img_name,pc.company_name,pa.name as agenter_name,pa.phone as agenter_phone,concat(pr.name,pra.name,prb.name) address_a');
            if (isset($param['store_name']) && !empty($param['store_name'])) {
                $store_model->where('store_name', 'like', '%' . $param['store_name'] . '%');
            }
            $store_model->join('cmf_petsun_company pc', 'pc.id=s.company_id', 'left');
            $store_model->join('cmf_petsun_agenter pa', 'pa.id=s.agenter_id and pa.delete_time=0', 'left');
            $store_model->join('cmf_petsun_imgs pi', 'FIND_IN_SET(pi.id, s.store_imgs)', 'left');
            $store_model->join('cmf_petsun_region pr', 'pr.id=s.province_id', 'left');
            $store_model->join('cmf_petsun_region pra', 'pra.id=s.city_id', 'left');
            $store_model->join('cmf_petsun_region prb', 'prb.id=s.area_id', 'left');
            $store_model->where($where);
            $store_model->group('s.id');
            $store_model->order(['s.id' => 'desc']);
            $store_buildSql = $store_model->buildSql();
            $store_model->table($store_buildSql . ' a')
                ->field('a.*,(IFNULL(sum(gs.sale_num),0)) num,(IFNULL(sum(gs.give_num),0)) song_num')
                ->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=a.id', 'left')
                ->group('a.id')->order(['a.id' => 'desc']);
            $store_buildSql = $store_model->buildSql();
            $store_model->table($store_buildSql . ' a')
                ->field('a.*,(IFNULL(sum(ags.sale_num),0)+a.num) num,(IFNULL(sum(ags.give_num),0)+a.song_num) song_num')
                ->join('cmf_petsun_agenter_goods_sale ags', 'ags.store_id=a.id', 'left')
                ->group('a.id')->order(['a.id' => 'desc']);
            //$store_buildSql = $store_model->buildSql();
            $list = $store_model->paginate(10);*/
        $list->appends($param);
        $new_data = $list->toArray()['data'];
        $store_imgs = array_column($new_data, 'store_imgs');
        $store_imgs_in = implode(',', array_filter($store_imgs));
        $ImgsModel = new ImgsModel();
        $ImgsModel->where('id', 'in', explode(',', $store_imgs_in));
        $imgs_list = $ImgsModel->column('id,img_name');
        foreach ($list as $k => $v) {
            $v->img_name = '';
            if (!empty($v->store_imgs)) {
                //获取第一张图片
                $v->img_name = $imgs_list[explode(',', $v->store_imgs)[0]];
            }
            $store_id = $v->id;
            $param = [];
            $param['store_id'] = $store_id;
            $param['is_use'] = 1;
            $param['delete_time'] = 0;
            $total = $clerkModel->getCntByParam($param);
            $list[$k]->clerk_num = $total;
        }
        return $list;
    }

    /**
     * 添加医院
     * @param array $data 页面数据
     * @return $this
     */
    public function addstore($data)
    {
        $store_model = new StoreModel();
        $imags = [];
        foreach ($data['store_imgs'] as $k => $v) {
            if (!empty($v)) {
                $imags[] = $store_model->upImg($v);
            }
        }
        $data ['created_at'] = date('Y-m-d H:i:s');
        $data ['store_imgs'] = implode(',', $imags);
        $data ['check_status'] = 1;
        if (empty($data['clerk_phone'])) {
            return '店长手机号不能为空';
        }
        if (!preg_match('/^1([0-9]{9})/', $data['clerk_phone'])) {
            return '店长手机号格式不正确';
        }
        if (empty($data['lat']) || empty($data['lon'])) {
            return '请选择正确的医院地址';
        }
        if (empty($data['province_id']) || empty($data['city_id'])) {
            return '省市区名称不能为空';
        }
        $store_model->where(function ($query) use ($data) {
            $query->where(['delete_time' => 0]);
            $query->where(['lat' => $data['lat'], 'lon' => $data['lon']]);
        });
        $store_model->whereOr(function ($query) use ($data) {
            $query->where(['delete_time' => 0]);
            $query->where(['address_detail' => trim($data['address_detail'])]);
        });
        $info = $store_model->find();
        if ($info) {
            return '该医院已经注册!';
        }
        if (!empty($data['feature_ts'])) {
            $data['ts'] = implode('_', $data['feature_ts']);
        }
        $lat_lon = bd_to_tx($data['lat'], $data['lon']);
        $data['lat'] = $lat_lon['lat'];
        $data['lon'] = $lat_lon['lng'];
        $data['create_time'] = time();
        $store_model->allowField(true)->data($data, true)->save();
        $this->addStoreMananger($data['clerk_phone'], $data['clerk_name'], $store_model->id);
        return true;
    }

    /**
     * 编辑医院
     * @param array $data 页面数据
     * @return $this
     */
    public function editstore($data)
    {
        $store_model = new StoreModel();

        $imgs = [];
        foreach ($data['store_imgs'] as $v) {
            $id = $store_model->upImg($v);
            if (empty($id)) {
                continue;
            }
            $imgs[] = $id;
        }
        $data['store_imgs'] = implode(',', $imgs);
        if (empty($data['clerk_phone'])) {
            return '店长手机号不能为空';
        }
        if (!preg_match('/^1([0-9]{9})/', $data['clerk_phone'])) {
            return '店长手机号格式不正确';
        }
        if (empty($data['lat']) || empty($data['lon'])) {
            return '请选择正确的医院地址';
        }
        if (empty($data['province_id']) || empty($data['city_id'])) {
            return '省市区名称不能为空';
        }
        if (!empty($data['feature_ts'])) {
            $data['ts'] = implode('_', $data['feature_ts']);
        }
        $lat_lon = bd_to_tx($data['lat'], $data['lon']);
        $data['lat'] = $lat_lon['lat'];
        $data['lon'] = $lat_lon['lng'];
        $store_model->allowField(true)->isUpdate(true)->data($data, true)->save();
        $this->addStoreMananger($data['clerk_phone'], $data['clerk_name'], $data['id']);
        return true;

    }

    /**
     * 添加或更新店长信息
     * @Author Zed
     * @Time 2019/3/25 12:06
     */
    public function addStoreMananger($phone = '', $name = '', $store_id = 0)
    {
        $agenterModel = new AgenterModel();
        $clerkModel = new ClerkModel();
        $storeModel = new StoreModel();

        if (empty($phone) || empty($name) || empty($store_id)) {
            return '手机号码|名称不能为空';
        }

        // 获取代理商信息
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if ($agenter) {
            return '该号码已注册成为代理商，不能再成为店长';
        }

        // 获取用户信息
        $param = [];
        $param['store_id'] = $store_id;
        $param['position'] = 2;
        $param['delete_time'] = 0;
        $clerk = $clerkModel->getOneByParam($param);
        if ($clerk['phone'] == $phone) {
            return;
        }
        if ($clerk) {
            $editData = ['delete_time' => time()];
            $clerkModel->updateById($editData, $clerk['id']);
        }
        $addDtata = [
            'store_id' => $store_id,
            'name' => $name,
            'phone' => $phone,
            'position' => 2,
            'delete_time' => 0,
            'created_at' => time(),
            'openid' => '',
        ];
        $new_clerk_id = $clerkModel->add($addDtata);

        $StoreGoodsSaleModel = new StoreGoodsSaleModel();

        $StoreGoodsSaleModel->save(['clerk_id' => $new_clerk_id], ['clerk_id' => $clerk['id'], 'store_id' => $store_id]);
        $storeModel->updateById(['store_owner_id' => $new_clerk_id], $store_id);
    }

    /**
     * 获取医院未审核列表
     */
    public function getRegChecksList($param)
    {
        $store_model = new StoreModel();
        $store_model->alias('s');
        $where = ['s.delete_time' => 0];
        $store_model->field('s.*,pi.img_name,concat(pr.name,pra.name,prb.name) address');
        $store_model->join('cmf_petsun_imgs pi', 'FIND_IN_SET(pi.id, s.store_imgs)', 'left');
        $store_model->join('cmf_petsun_region pr', 'pr.id=s.province_id', 'left');
        $store_model->join('cmf_petsun_region pra', 'pra.id=s.city_id', 'left');
        $store_model->join('cmf_petsun_region prb', 'prb.id=s.area_id', 'left');
        $store_model->where($where);
        $store_model->where('s.check_status', 'neq', '1');
        $store_model->group('s.id');
        $list = $store_model->order(['s.check_status' => 'asc', 's.id' => 'ASC'])->paginate(10);
        $list->appends($param);
        return $list;
    }

    /**
     * 获取医院代理信息
     */
    public function getStoreAgenterInfo($param)
    {
        $store_model = new StoreModel();
        $store_model->alias('s');
        $store_model->field('s.*,a.name agenter_name,a.phone agenter_phone,IFNULL(sum(gs.sale_num),0) as num,IFNULL(sum(gs.give_num),0) as give_num');
        $store_model->join('cmf_petsun_agenter_goods_sale gs', 'gs.agenter_id=s.agenter_id and gs.store_id=s.id', 'left');
        $store_model->join('cmf_petsun_agenter a', 'a.id=s.agenter_id', 'left');
        $where = ['s.id' => $param['id'], 's.delete_time' => 0];
        $info = $store_model->where($where)->find();
        return $info;
    }

}