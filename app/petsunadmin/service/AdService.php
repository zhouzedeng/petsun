<?php

namespace app\petsunadmin\service;


use app\comm\model\AdModel;

class AdService
{
    /**
     * 获取广告列表
     */
    public function getAdList($param)
    {
        $admodel = new AdModel();
        $admodel->alias('a');
        $admodel->join('cmf_petsun_imgs pi', 'pi.id=a.img_url','left');
        $list = $admodel->field('a.*,pi.img_name as img_url')
            ->order( ['a.id' => 'ASC'])
            ->paginate();
        return $list;
    }
    /**
     * 添加广告
     * @param array $data 页面数据
     * @return $this
     */
    public function addad($data)
    {
        $admodel = new AdModel();
        $data ['created_at'] = date('Y-m-d H:i:s');
        $img_id = $admodel ->upImg($data['img_url']);
        $data['img_url'] = $img_id;
        $res = $admodel->allowField(true)->data($data, true)->save();
        return $res;

    }

}