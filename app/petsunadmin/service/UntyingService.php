<?php

namespace app\petsunadmin\service;


use app\comm\model\AdModel;
use app\comm\model\StoreModel;

class UntyingService
{
    /**
     * 获取解绑列表
     */
    public function getList()
    {
        $pid = session('pid');
        $where = [];
        if ($pid == 3) {
            $where = [
                's.company_id' => $pid
            ];
        }
        $store_model = new StoreModel();
        $store_model->alias('s');
        $store_model->field('pco.company_name,pc.phone clerk_phone,pc.name clerk_name,pc.position,s.store_name,pul.id pul_id,pul.check_desc,pul.refuse_desc,pul.opinion,pul.id,pul.no_use_desc,pul.no_use_imgs,pul.unbind_phone,pul.unbind_name');
        $store_model->where($where);
        $store_model->join('cmf_petsun_unbind_log pul', 'pul.store_id=s.id');
        $store_model->join('cmf_petsun_clerk pc', 'pc.id=s.store_owner_id');
        $store_model->join('cmf_petsun_company pco', 'pco.id=s.company_id');
        $store_model->order('pul.status', 'asc');
        $list = $store_model->paginate(10);
        return $list;
    }

}