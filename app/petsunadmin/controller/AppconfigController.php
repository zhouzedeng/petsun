<?php

namespace app\petsunadmin\controller;

use app\comm\model\AgenterModel;
use app\comm\model\ImgsModel;
use app\comm\model\StoreModel;
use app\comm\model\UnbindLogModel;
use app\petsunadmin\service\UntyingService;
use cmf\controller\AdminBaseController;

class AppconfigController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 小程序配置
     */
    public function index()
    {
        if (session('user_type') == 3) {
            return '您没有权限配置';
        }
        $param = $this->request->param();
        if (request()->isPost()) {
            $param = $param['post'];
            $appconfig = ['service_phone' => $param['service_phone'], 'xcx_img' => $param['xcx_img']];
            cache('appconfig', $appconfig);
            $this->success('修改成功', url('Appconfig/index'));
        }
        $appconfig = cache('appconfig');
        if (empty($appconfig)) {
            //service_phone是客服电话,xcx_img是分享图片
            $appconfig = ['service_phone' => '', 'xcx_img' => ''];
            cache('appconfig', $appconfig);
        }
        $this->assign('appconfig', $appconfig);

        return $this->fetch();
    }
}