<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 2019/3/2
 * Time: 16:00
 * 代理公司列表
 */

namespace app\petsunadmin\controller;

use app\admin\model\UserModel;
use app\comm\model\CompanyModel;
use app\petsunadmin\service\AgenterService;
use app\petsunadmin\service\CompanyService;
use app\petsunadmin\service\StoreService;
use cmf\controller\AdminBaseController;
use think\Db;

class CompanyController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 代理公司列表
     */
    public function index()
    {
        $param = $this->request->param();
        $company_service = new CompanyService();
        $data = $company_service->getList($param);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        return $this->fetch();
    }

    /**
     * 添加代理公司
     */
    public function add()
    {
        if (request()->isPost()) {
            $data = $this->request->param();
            $ad_service = new CompanyService();
            if (isMobile($data['post']['ceo_phone']) == false) {
                $this->error('请输入正确的手机号');
            }
            $res = $ad_service->addCompany($data['post']);
            if ($res === true) {
                $this->success('添加成功', url('Company/index'));
            } else {
                $this->error($res);
            }
        }
        return $this->fetch();
    }

    /**
     * 编辑代理公司
     */
    public function edit()
    {
        $data = $this->request->param();
        $company_model = new CompanyModel();
        if ($this->request->isPost()) {
            $user_model = new UserModel();
            $user_model->where(['user_login'=>$data['post']['ceo_phone']]);
            $user_model->where('pid','neq',$data['post']['id']);
            $in_user = $user_model->find();
            if($in_user){
                $this->error('手机号已经绑定其他代理商');
            }
            $user_model->validate('admin/User.edit')->save(['user_login'=>$data['post']['ceo_phone']],['pid'=>$data['post']['id']]);
            $res = $company_model->allowField(true)->isUpdate(true)->data($data['post'], true)->save();
            if ($res) {
                $this->success('修改成功', url('Company/index'));
            } else {
                $this->error('修改失败', url('Company/index'));
            }
        } else {
            $info = $company_model->where(['id' => $data['id']])->find();
            $this->assign('post', $info);
        }
        return $this->fetch();
    }

    /**
     * 删除代理公司
     */
    public function delete()
    {
        $data = $this->request->param();
        $company_service = new CompanyService();
        $result = $company_service->deleteCompany($data);
        if ($result) {
            $this->success(lang('删除成功'), url('Company/index'));
        } else {
            $this->error(lang('删除失败'), url('Company/index'));
        }
    }

    /**
     *  代理商详情
     */
    public function details()
    {
        $data = $this->request->param();
        $company_service = new CompanyService();
        $result = $company_service->details($data);
        $info = $result['info'];
        $agenter_list = $result['agenter_list'];
        $this->assign('post', $info);
        $this->assign('agenter_list', $agenter_list->items());
        $this->assign('page', $agenter_list->render());
        return $this->fetch();
    }

    /**
     *  代理人详情
     */
    public function agenterDetails()
    {
        $data = $this->request->param();
        $company_service = new CompanyService();
        $result = $company_service->agenterDetails($data);
        $info = $result['info'];
        $store_list = $result['store_list'];
        $this->assign('post', $info);
        $this->assign('clerk_url', 'petsunadmin/company/clerk');
        $this->assign('store_list', $store_list->items());
        $this->assign('page', $store_list->render());
        return $this->fetch();
    }

    /**
     *  人员列表
     */
    public function clerk()
    {
        $data = $this->request->param();
        $company_service = new CompanyService();
        $result = $company_service->clerkList($data);
        $store_model = new StoreService();
        $agenter_info = $store_model->getStoreAgenterInfo($data);
        $this->assign('clerk_list', $result);
        $this->assign('agenter_info', $agenter_info);
        return $this->fetch();
    }

    public function getaCity()
    {

        $list = Db::table('cmf_petsun_region')->order('pid', 'asc')->column('id,name,pid');
        $pid = [];
        foreach ($list as $k => $v) {
            if ($v['pid'] == 0) {
                $pid [$v['id']] = $v;
            }
            if (!empty($pid [$v['pid']]['id']) && $v['pid'] == $pid [$v['pid']]['id']) {
                $pid [$v['pid']] ['city'] [$v['id']] = $v;
            }
        }
        foreach ($pid as $k1 => $v1) {
            foreach ($list as $k3 => $v3) {
                if (!empty($v1['city'][$v3['pid']]) && $v3['pid'] != 0 && $v1['city'][$v3['pid']]['id'] == $v3['pid']) {
                    $pid [$k1]['city'][$v3['pid']]['area'][$k3] = $v3;
                }
            }
        }
    }
    /**
     * 公司资料
     */
    public function info(){
        $user_type = session('user_type');
        if($user_type !=3){
            echo '您不是代理公司管理帐号';
            die;
        }
        $company_model = new CompanyModel();
        $info = $company_model->where(['id'=>session('pid')])->find();

        $this->assign('info', $info);
        return $this->fetch();
    }
}