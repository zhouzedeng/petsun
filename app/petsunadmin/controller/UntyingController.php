<?php

namespace app\petsunadmin\controller;

use app\comm\model\AgenterModel;
use app\comm\model\ImgsModel;
use app\comm\model\StoreModel;
use app\comm\model\UnbindLogModel;
use app\petsunadmin\service\UntyingService;
use cmf\controller\AdminBaseController;

class UntyingController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 申请解绑列表
     */
    public function index()
    {
        $untying_service = new UntyingService();
        $data = $untying_service->getList();
        $list = [];
        foreach ($data->toArray()['data'] as $k => $v) {
            $list [$k] = $v;
            $imgs = explode(',', $v['no_use_imgs']);
            $list [$k]['imgs'] = !empty($imgs) ? $imgs[0] : '';
            $imgModel = new ImgsModel();
            $img = $imgModel->getOneByParam(['id' =>  $list [$k]['imgs']]);
            if ($img) {
                $list [$k]['imgs'] = $img['img_name'];
            }
        }

        $agener_model = new AgenterModel();
        $agener_list = $agener_model->where(['company_id'=>session('pid')])->select();
        $this->assign('user_type', session('user_type'));
        $this->assign('agener_list', $agener_list);
        $this->assign('list', $list);
        $this->assign('page', $data->render());
        return $this->fetch();
    }

    /**
     * 拒绝申请
     */
    public function refuse()
    {
        $param = $this->request->param();
        if (empty($param['refuse_desc'])) {
            $this->error('拒绝原因不能为空');
        }
        $unbind_log_model = new UnbindLogModel();
        $unbind_log_model->save(['opinion' => 2, 'refuse_desc' => $param['refuse_desc'], 'status' => 2], ['id' => $param['id']]);
        $this->success('拒绝成功');

    }
    /**
     * 同意解绑换新代理
     */
    public function Adopt(){
        $param = $this->request->param();
        if(empty($param['agener_id'])){
            $this->error('请选择新代理');
        }
        $unbind_log_model = new UnbindLogModel();
        $unbind_info = $unbind_log_model->where(['id'=>$param['id']])->find();
        $unbind_log_model->save(['opinion' => 1, 'status' => 2,'new_agenter_id'=>$param['agener_id']], ['id' => $param['id']]);
        $store_model  = new StoreModel();
        $res = $store_model->save(['agenter_id' => $param['agener_id']],['id'=>$unbind_info->store_id]);
        $this->success('解绑成功');
    }
}