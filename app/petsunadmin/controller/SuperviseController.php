<?php

namespace app\petsunadmin\controller;

use app\comm\model\GoodsModel;
use app\petsunadmin\service\SuperviseService;
use cmf\controller\AdminBaseController;

class SuperviseController extends AdminBaseController
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 监管管理列表
     */
    public function index()
    {
        $param = $this->request->param();
        $ad_service = new SuperviseService();
        $data = $ad_service->getList($param);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        return $this->fetch();
    }

    /**
     * 编辑监管码
     */
    public function edit()
    {
        $data = $this->request->param();
        $goods_model = new GoodsModel();
        if ($this->request->isPost()) {
            $data['post']['scan_time'] = time();
            $res = $goods_model->allowField(true)->isUpdate(true)->data($data['post'], true)->save();
            $this->success('修改成功', url('supervise/index'));
        } else {
            $info = $goods_model->where(['id' => $data['id']])->find();
            $this->assign('post', $info);
        }
        return $this->fetch();
    }

    /*
     * 处理监管码
     */
    public function upcode()
    {
        $param = $this->request->param();
        if (empty($param["filepath"])) {
            $this->error('请先上传监管码');
        }
        // 如果是监管码菜单发来的上传xml文件，则需进行数据解析
        if (cmf_get_file_extension(cmf_get_file_download_url($param["filepath"])) == 'xml') {
            $content = file_get_contents(cmf_get_file_download_url($param["filepath"]));
            $obj = simplexml_load_string($content);
            $arr = obj2arr($obj);
            if (isset($arr['Product']) && isset($arr['Product']['Batch'])) {
                if (empty($arr['Product']['@attributes'])) {
                    $this->error('xml格式错误');
                }
                $add_data = [
                    'goods_serial' => '',
                    'goods_parent_serial' => '',
                    'goods_name_code' => empty($arr['Product']['@attributes']['productName']) ? '' : $arr['Product']['@attributes']['productName'],
                    'goods_name' => empty($arr['Product']['@attributes']['productName']) ? '' : $arr['Product']['@attributes']['productName'],
                    'batchNo' => empty($arr['Product']['Batch']['@attributes']['batchNo']) ? '' : empty($arr['Product']['Batch']['@attributes']['batchNo']),
                    'scan_status' => 0,
                    'agree_no' => empty($arr['Product']['@attributes']['pzwh']) ? '' : $arr['Product']['@attributes']['pzwh'],
                    'useful' => 1,
                    'created_at' => time(),
                    'type' => 0,
                ];

                $codeList = empty($arr['Product']['Batch']['Data']) ? [] : $arr['Product']['Batch']['Data'];
                $tmp = [];

                $goodsModel = new GoodsModel();
                $tagRatio = $arr['Product']['Batch']['@attributes']['tagRatio'];
                $arr = explode(':', $tagRatio);
                $tagPackRatio = count($arr);


                $i = 1;
                $j = 0;
                foreach ($codeList as $k => $v) {
                    $codeStr = empty($v['@attributes']['code']) ? '' : $v['@attributes']['code'];
                    $codeArr = explode('，', $codeStr);
                    $code = $codeArr ? $codeArr[0] : '';
                    if ($tagPackRatio == 1) {
                        $code_parent = '';
                        $add_data['type'] = 1;
                        $add_data['goods_num'] = 1;
                    } elseif ($tagPackRatio == 2) {
                        if ($i % 7 == 0) {
                            $code_parent = '';
                            $add_data['goods_num'] = 6;
                            $add_data['type'] = 2;
                        } else {
                            $p_key = ceil($i / 7) * 7;
                            //key下标从0开发，所以减1，
                            $p_key -= 1;
                            $code_p = empty($codeList[$p_key]['@attributes']['code']) ? '' : $codeList[$p_key]['@attributes']['code'];
                            $code_parent = $code_p;
                            $add_data['goods_num'] = 1;
                            $add_data['type'] = 1;
                        }
                    } elseif ($tagPackRatio == 3) {
                        if ($i % 29 == 0) {
                            $code_parent = '';
                            $add_data['goods_num'] = 24;
                            $add_data['type'] = 3;
                            //遍历完大盒码j加1,$i恢复0
                            $i = 0;
                            $j++;
                        } else if ($i % 7 == 0) {
                            //29是第一个大盒码key,($j * 29)是已经遍历完的大盒码
                            $p_2_key = 29 + ($j * 29);
                            //key下标从0开发，所以减1，
                            $p_2_key -= 1;
                            $code_p_2 = empty($codeList[$p_2_key]['@attributes']['code']) ? '' : $codeList[$p_2_key]['@attributes']['code'];
                            $code_parent = $code_p_2;
                            $add_data['goods_num'] = 6;
                            $add_data['type'] = 2;
                        } else {
                            //$p_1_key是获取中盒码code,i除7在向上取整在乘7(7是中盒所在位置),在加上每个大盒码的29乘已遍历完的数量
                            $p_1_key = (ceil($i / 7) * 7) + ($j * 29);
                            //key下标从0开发，所以减1，
                            $p_1_key -= 1;
                            $code_p_1 = empty($codeList[$p_1_key]['@attributes']['code']) ? '' : $codeList[$p_1_key]['@attributes']['code'];
                            $code_parent = $code_p_1;
                            $add_data['goods_num'] = 1;
                            $add_data['type'] = 1;
                        }
                    }
                    $i++;
                    $add_data['goods_serial'] = $code;
                    $add_data['goods_parent_serial'] = $code_parent;
                    $tmp[] = $add_data;
                }
                $goodsModel->addList($tmp);
            }
            $this->success('提交成功', url('Supervise/index'));
        } else {
            $this->error('上传格式为XML');
        }
    }
}