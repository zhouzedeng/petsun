<?php
/**
 * 医院管理
 */

namespace app\petsunadmin\controller;


use app\comm\model\AdModel;
use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\CompanyModel;
use app\comm\model\FeatureModel;
use app\comm\model\StoreModel;
use app\petsunadmin\service\AdService;
use app\petsunadmin\service\CompanyService;
use app\petsunadmin\service\StoreService;
use cmf\controller\AdminBaseController;
use think\Debug;

class StoreController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 医院管理列表
     */
    public function index()
    {
        $param = $this->request->param();
        $store_service = new StoreService();

        $data = $store_service->getStoreList($param);
        $list = [];
        foreach ($data->toArray()['data'] as $k => $v) {
            $list [$k] = $v;
            $imgs = explode(',', $v['img_name']);
            $list [$k]['imgs'] = !empty($imgs) ? $imgs[0] : '';
        }

        $this->assign('clerk_url', 'petsunadmin/company/clerk');
        $this->assign('list', $list);
        $this->assign('user_type', session('user_type'));
        $this->assign('page', $data->render());
        $this->assign('currentPage', $data->currentPage());
        return $this->fetch();
    }

    /**
     * 添加医院
     */
    public function add()
    {

        if (request()->isPost()) {
            $data = $this->request->param();
            $ad_service = new StoreService();
            $res = $ad_service->addstore($data['post']);
            if (is_string($res)) {
                $this->error($res);
            } else {
                $this->success('添加成功', url('store/index'));
            }
        }
        $feature_model = new FeatureModel();
        $feature_list = $feature_model->select();
        $user_type = session('user_type');
        $pid = session('pid');
        $where = [];
        $agenter_model = new AgenterModel();
        $agenter_list = [];
        if ($user_type == 3) {
            $where = ['id' => $pid];
            $agenter_list = $agenter_model->where(['company_id' => $pid, 'delete_time' => 0])->select();
        }
        $compans_model = new CompanyModel();
        $compans_model->where($where);
        $compans_model->where(['delete_time' => 0]);
        $company_list = $compans_model->select();
        $this->assign('agenter_list', $agenter_list);
        $this->assign('company_list', $company_list);
        $this->assign('company_id', $pid);
        $this->assign('user_type', $user_type);
        $this->assign('feature_list', $feature_list);
        return $this->fetch();
    }

    /**
     * 编辑医院
     */
    public function edit()
    {
        $data = $this->request->param();
        if ($this->request->isPost()) {
            $store_service = new StoreService();
            $res = $store_service->editstore($data['post']);
            if (is_string($res)) {
                $this->error($res);
            } else {
                header('Location:index?page='.$data['post']['page']);
                die;
                //$this->redirect('store/index?page=' . $data['post']['page']);
               //$this->success('修改成功', url('store/index', ['page' => $data['post']['page']]));
            }
        }
        $store_model = new StoreModel();
        $store_model->alias('s');
        $store_model->field('s.*,group_concat(pi.img_name) as img_name');
        $store_model->join('cmf_petsun_imgs pi', 'FIND_IN_SET(pi.id, s.store_imgs)', 'left');
        $store_model->group('s.id');
        $store_info = $store_model->where(['s.id' => $data['id']])->find();

        $feature_model = new FeatureModel();
        $feature_list = $feature_model->select();
        $where = [];
        $pid = '';
        $compans_model = new CompanyModel();
        if (session('user_type') == 3) {
            $pid = session('pid');
            $where = ['id' => $pid];

        }
        $compans_model->where($where);
        $compans_model->where(['delete_time' => 0]);
        $company_list = $compans_model->select();

        $agenter_model = new AgenterModel();
        $agenter_list = $agenter_model->where(['company_id' => $store_info['company_id'], 'delete_time' => 0])->select();

        $clerk_model = new ClerkModel();
        $clerk_info = $clerk_model->where(['id' => $store_info['store_owner_id'], 'delete_time' => 0])->find();
        $store_info ['clerk_name'] = $clerk_info['name'];
        $store_info ['clerk_phone'] = $clerk_info['phone'];

        $store_info ['img_name'] = empty($store_info ['img_name']) ? '' : explode(',', $store_info ['img_name']);
        $store_info ['store_imgs'] = explode(',', $store_info ['store_imgs']);
        if (empty($store_info ['img_name']) || count($store_info ['store_imgs']) != count($store_info ['img_name'])) {
            $store_info ['imgs'] = '';
        } else {
            $store_info ['imgs'] = array_combine($store_info ['store_imgs'], $store_info ['img_name']);
        }
        $store_info ['ts'] = explode('_', $store_info['ts']);
        $lat_lon = tx_to_bd($store_info['lat'], $store_info['lon']);
        $store_info['lat'] = $lat_lon['lat'];
        $store_info['lon'] = $lat_lon['lng'];
        $this->assign('currentPage', $data['page']);
        $this->assign('company_id', $pid);
        $this->assign('post', $store_info);
        $this->assign('user_type', session('user_type'));
        $this->assign('company_list', $company_list);
        $this->assign('agenter_list', $agenter_list);
        $this->assign('feature_list', $feature_list);
        return $this->fetch();
    }

    /**
     * 删除医院
     */
    public function delete()
    {
        $data = $this->request->param();
        $store_model = new StoreModel();
        $clerk_model = new ClerkModel();
        $store_res = $store_model->save(['delete_time' => time()], ['id' => $data['id']]);
        $clerk_res = $clerk_model->save(['delete_time' => time()], ['store_id' => $data['id']]);
        $this->success(lang('删除成功'));
    }


    /**
     *  医院特色编辑
     */
    public function feature()
    {
        $feature_model = new FeatureModel();
        $list = $feature_model->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 新增特色
     */
    public function featureadd()
    {
        $data = $this->request->param();
        $ad_service = new FeatureModel();
        $data['post'] ['created_at'] = date('Y-m-d H:i:s');
        $res = $ad_service->save($data['post']);
        if ($res == true) {
            $this->success('添加成功', url('store/feature'));
        } else {
            $this->error('添加失败', url('store/feature'));
        }
    }

    /**
     * 删除特色
     */
    public function featuredel()
    {
        $data = $this->request->param();
        $feature_model = new FeatureModel();
        $result = $feature_model->where(['id' => $data['id']])->delete();
        if ($result) {
            $this->success('删除成功', url('store/feature'));
        } else {
            $this->error('删除失败', url('store/feature'));
        }
    }

    /**
     * 编辑特色
     */
    public function featureedit()
    {
        $data = $this->request->param();
        $feature_model = new FeatureModel();
        $result = $feature_model->allowField(true)->isUpdate(true)->data($data['post'], true)->save();
        if ($result) {
            $this->success('修改成功', url('store/feature'));
        } else {
            $this->error('修改失败', url('store/feature'));
        }
    }

    /**
     * 代理公司获取代理人列表
     */
    public function getAgenterList()
    {
        $data = $this->request->param();
        $agenter_model = new AgenterModel();
        $agenter_list = $agenter_model->where(['company_id' => $data['id'], 'delete_time' => 0])->select();
        return json($agenter_list);
    }

    /**
     * 注册审核
     */
    public function regCheck()
    {
        $param = $this->request->param();
        $store_service = new StoreService();
        $data = $store_service->getRegChecksList($param);
        $company_service = new CompanyService();
        $company_list = $company_service->getCompanyList();
        $this->assign('company_list', $company_list);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        return $this->fetch();
    }

    /**
     * 审核通过
     */
    public function checkAdopt()
    {
        $param = $this->request->param();
        $store_model = new StoreModel();
        if (empty($param['company_id'])) {
            //$this->error('代理商不能为空');
        }
        $store_model->save(['check_status' => 1, 'company_id' => $param['company_id']], ['id' => $param['id']]);
        $this->success('审核成功');
    }

    /**
     * 审核拒绝
     */
    public function checkRefuse()
    {
        $param = $this->request->param();
        $store_model = new StoreModel();
        if (empty($param['check_reason'])) {
            $this->error('拒绝不能为空');
        }
        $store_model->save(['check_status' => 2, 'check_reason' => $param['check_reason']], ['id' => $param['id']]);
        $this->success('拒绝成功');
    }
}