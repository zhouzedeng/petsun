<?php

namespace app\petsunadmin\controller;

use app\comm\model\AgenterModel;
use app\petsunadmin\service\AgenterService;
use cmf\controller\AdminBaseController;

/**
 * Class UntyingController
 * @package app\petsunadmin\controller
 * 解绑代理
 */
class AgenterController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 代理管理列表
     */
    public function index()
    {
        $ad_service = new AgenterService();
        $pid = session('pid');
        if (session('user_type') != 3) {
            echo '您不是代理公司管理帐号';
            die;
        }
        $data = $ad_service->getList(['pid' => $pid]);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        return $this->fetch();
    }

    /**
     * 添加代理
     */
    public function add()
    {

        if (request()->isPost()) {
            $data = $this->request->param();
            $agenter_service = new AgenterService();
            if (!isMobile($data['post']['phone'])) {
                $this->error('请输入正确的手机号');
            }
            $res = $agenter_service->add($data['post']);
            if (is_string($res)) {
                $this->error($res);
            } else {
                $this->success('添加成功', url('Agenter/index'));
            }
        }
        return $this->fetch();
    }

    /**
     * 编辑代理
     */
    public function edit()
    {
        $data = $this->request->param();
        $agenter_model = new AgenterModel();
        $pid = session('pid');
        if ($this->request->isPost()) {
            if(!isMobile($data['post']['phone'])){
                $this->error('请输入正确的手机号');
            }
            $updatadata = [
                'phone'=> $data['post']['phone'],
                'name'=> $data['post']['name'],
            ];
            $res = $agenter_model->save($updatadata,['company_id'=>$pid,'id'=>$data['post']['id']]);
            $this->success('修改成功', url('agenter/index'));
        } else {
            $info = $agenter_model->where(['id' => $data['id'],'company_id'=>$pid])->find();
            $this->assign('post', $info);
        }
        return $this->fetch();
    }

    /**
     * 删除代理
     */
    public function delete()
    {
        $data = $this->request->param();
        $agenter_model = new AgenterModel();
        $result = $agenter_model->save(['delete_time'=>time()],['id'=>$data['id'],'company_id'=>session('pid')]);
        if ($result) {
            $this->success(lang('删除成功'));
        } else {
            $this->error(lang('删除失败'));
        }
    }
}