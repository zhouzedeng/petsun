<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 2019/3/1
 * Time: 22:54
 * 广告管理
 */

namespace app\petsunadmin\controller;


use app\comm\model\AdModel;
use app\petsunadmin\service\AdService;
use cmf\controller\AdminBaseController;

class CouponController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 优惠券规则管理
     */
    public function index()
    {
        $param = $this->request->param();
        if (request()->isPost()) {
            $param = $param['post'];
            $couponrule = [
                ['BuyNumber' => $param[1]['BuyNumber'], 'GiveNumber' => empty($param[1]['GiveNumber'])?1:$param[1]['GiveNumber'], 'type' => 1, 'Limit' => $param[1]['Limit']],
                ['BuyNumber' => $param[2]['BuyNumber'], 'GiveNumber' => empty($param[2]['GiveNumber'])?1:$param[2]['GiveNumber'], 'type' => 2, 'Limit' => 0]
            ];
            cache('couponrule', $couponrule);
            $this->success('修改成功', url('Coupon/index'));
        }
        $couponrule = cache('couponrule');
        if (empty($couponrule)) {
            //BuyNumber购买数量，GiveNumber赠送数量，type:1是首次，2是二次，Limit是上限数，0不限
            $couponrule = [
                ['BuyNumber' => 1, 'GiveNumber' => 1, 'type' => 1, 'Limit' => 3],
                ['BuyNumber' => 2, 'GiveNumber' => 1, 'type' => 2, 'Limit' => 0]
            ];
            cache('couponrule', $couponrule);
        }
        $this->assign('couponrule', $couponrule);
        return $this->fetch();
    }

}