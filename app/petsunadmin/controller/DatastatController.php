<?php

namespace app\petsunadmin\controller;

use app\comm\model\AgenterGoodsSaleModel;
use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\CompanyModel;
use app\comm\model\ProductModel;
use app\comm\model\StoreModel;
use app\petsunadmin\service\UntyingService;
use cmf\controller\AdminBaseController;
use think\Db;

class DatastatController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 数据统计
     */
    public function index()
    {
        $user_type = session('user_type');
        $pid = session('pid');
        $store_model = new StoreModel();
        $clerk_model = new ClerkModel();
        $agenter_model = new AgenterModel();
        $company_model = new CompanyModel();
        $clerk_model->alias('c');
        if ($user_type == 3) {
            $store_model->where(['company_id' => $pid,]);
            $agenter_model->where(['company_id' => $pid,]);
            $clerk_model->join('cmf_petsun_store ps', 'ps.check_status=1 and ps.delete_time=0 and ps.id=store_id and ps.company_id=' . $pid);
        }
        //医院数量
        $store_count = $store_model->where(['check_status' => 1, 'delete_time' => 0])->count();
        //医生数量
        $clerk_count = $clerk_model->count();
        //代理数量
        $agenter_count = $agenter_model->where(['delete_time' => 0,])->count();
        //代理公司数量
        $company_count = $company_model->where(['delete_time' => 0,])->count();
        $this->assign('store_count', $store_count);
        $this->assign('clerk_count', $clerk_count);
        $this->assign('agenter_count', $agenter_count);
        $this->assign('company_count', $agenter_count);
        $this->assign('user_type', $user_type);
        return $this->fetch();
    }

    /*
     * 下单统计
     */
    public function statOrder()
    {
        $param = $this->request->param();
        $type = isset($param['type']) ? $param['type'] : 0;
        $timewee = isset($param['timewee']) ? $param['timewee'] : 0;
        $sortsize = isset($param['sortsize']) ? $param['sortsize'] : 0;
        $sort_zd = isset($param['sort_zd']) ? $param['sort_zd'] : 0;
        if ($timewee) {
            Switch ($timewee) {
                case 1:
                    $param['start_time'] = date("Y-m-d", strtotime("-1 year"));
                    break;
                case 2:
                    $param['start_time'] = date("Y-m-d", strtotime("-3 month"));
                    break;
                case 3:
                    $param['start_time'] = date("Y-m-d", strtotime("-1 month"));
                    break;
                case 4:
                    $param['start_time'] = date("Y-m-d", strtotime("-1 week"));
                    break;
            }
            $param['end_time'] = date("Y-m-d H:i:s");
        }
        $store_model = new StoreModel();
        $store_model->alias('s');
        $store_model->field('s.*,IFNULL(sum(gs.sale_num),0) num,IFNULL(sum(IFNULL(pp.price,0)*gs.sale_num),0) total_price,IFNULL(sum(song_num),0) song_num');
        if (session('user_type') == 3) {
            $pid = session('pid');
            $where ['s.company_id'] = $pid;
            $store_model->where(['s.company_id'=> $pid]);
        }
        if ((!empty($param['start_time']) && empty($param['end_time'])) || (empty($param['start_time']) && !empty($param['end_time']))) {
            $this->error('请选择正确的时间段');
        }
        $on_where = '';
        $gs_on_where = '';
        if (!empty($param['start_time']) && !empty($param['end_time'])) {
            $start_time = strtotime($param['start_time']);
            $end_time = strtotime($param['end_time']);
            $on_where = ' and po.created_at>=' . $start_time . ' and po.created_at<=' . $end_time;
            $gs_on_where = ' and gs.created_at>="' . $param['start_time'] . '" and gs.created_at<="' . $param['end_time'] . '"';
            $this->assign('start_time', date('Y-m-d H:i', $start_time));
            $this->assign('end_time', date('Y-m-d H:i', $end_time));
        }
        $product_name = isset($param['product_name']) ? $param['product_name'] : '';
        $pp_where = '';
        if($product_name){
            $gs_on_where .= ' and gs.goods_name = "'.$product_name.'"';
            $pp_where .= ' and pp.name="'.$product_name.'"';
        }
        $store_model->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id' . $gs_on_where, 'left');
        $store_model->join('cmf_petsun_product pp', 'pp.delete_time=0 and pp.name=gs.goods_name ', 'left');
        $where = ['s.check_status' => 1, 's.delete_time' => 0];
        $store_model->group('s.id');
        $store_model->where($where);
        $store_buildSql = $store_model->buildSql();
        $ags_where = 'ags.store_id=a.id and ags.agenter_id=a.agenter_id';
        $pp_where = '';
        if($product_name){
            $ags_where .= ' and ags.goods_name="'.$product_name.'"';
            $pp_where .= ' and pp.name="'.$product_name.'"';
        }
        $store_model->table($store_buildSql . ' a')
            ->field('a.*,(IFNULL(sum(ags.sale_num),0)+a.num) num,(IFNULL(sum(IFNULL(pp.price,0)*ags.sale_num),0)+a.total_price) total_price,(IFNULL(sum(ags.give_num),0)+a.song_num) song_num,pp.name')
            ->join('cmf_petsun_agenter_goods_sale ags', $ags_where, 'left')
            ->join('cmf_petsun_product pp', 'pp.delete_time=0 and  pp.`name` = ags.`goods_name`', 'left')
            ->group('a.id');
        $order_field = 'num';
        if ($sort_zd == 1) {
            $order_field = 'num';
        }
        if ($sort_zd == 2) {
            $order_field = 'song_num';
        }
        if ($sort_zd == 3) {
            $order_field = 'total_price';
        }
        if ($sort_zd != 0) {
            if ($sortsize == 1) {
                $store_model->order($order_field, 'desc');
                $store_model->order('a.id', 'desc');
            }
            if ($sortsize == 2) {
                $store_model->order($order_field, 'asc');
                $store_model->order('a.id', 'desc');
            }
        }
        $data = $store_model->paginate(10);
        //查询产品
        $product_model = new ProductModel();
        $where = ['delete_time' => 0];
        $product_model->field('id,name');
        $product_model->where($where);
        $product_list = $product_model->select()->toArray();
        $this->assign('product_list', $product_list);
        $this->assign('product_name', $product_name);


        $data->appends($param);
        $this->assign('sort_zd', $sort_zd);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        $this->assign('timewee', $timewee);
        $this->assign('sortsize', $sortsize);
        $param ['type'] = 1;
        $this->assign('vanurl', url('datastat/statorder', $param));
        if (isset($type) && $type == 1) {
            return $this->fetch('stat_ordera');
        }
        return $this->fetch();
    }

    /*
     * 代理统计
     */
    public function statAgenter()
    {
        $param = $this->request->param();
        $agenter_model = new AgenterModel();
        $type = isset($param['type']) ? $param['type'] : 0;
        $timewee = isset($param['timewee']) ? $param['timewee'] : 0;
        $sortsize = isset($param['sortsize']) ? $param['sortsize'] : 0;
        $sort_zd = isset($param['sort_zd']) ? $param['sort_zd'] : 0;
        if ($timewee) {
            Switch ($timewee) {
                case 1:
                    $param['start_time'] = date("Y-m-d", strtotime("-1 year"));
                    break;
                case 2:
                    $param['start_time'] = date("Y-m-d", strtotime("-3 month"));
                    break;
                case 3:
                    $param['start_time'] = date("Y-m-d", strtotime("-1 month"));
                    break;
                case 4:
                    $param['start_time'] = date("Y-m-d", strtotime("-1 week"));
                    break;
            }
            $param['end_time'] = date("Y-m-d 23:59:59");
        }
        $agenter_model->alias('a');
        $agenter_model->field('a.*,pc.company_name,IFNULL(sum(gs.sale_num),0) num,IFNULL(sum(IFNULL(pp.price,0)*gs.sale_num),0) total_price');
        $where = ['a.delete_time' => 0];
        $user_type = session('user_type');
        if ($user_type == 3) {
            $pid = session('pid');
            $where ['a.company_id'] = $pid;
        }
        $on_where = '';
        $gs_on_where = '';
        if (!empty($param['start_time']) && !empty($param['end_time'])) {
            $start_time = strtotime($param['start_time']);
            $end_time = strtotime($param['end_time']);
            $on_where = ' and po.created_at>=' . $start_time . ' and po.created_at<=' . $end_time;
            $gs_on_where = ' and gs.created_at>="' . $param['start_time'] . '" and gs.created_at<="' . $param['end_time'] . '"';
            $this->assign('start_time', date('Y-m-d H:i', $start_time));
            $this->assign('end_time', date('Y-m-d H:i', $end_time));
        }
        //添加产品条件
        $product_id = isset($param['product_id']) ? $param['product_id'] : 0;
        if ($product_id != 0) {
            $agenter_model->where(['pp.id' => $product_id]);
            #$on_where .= ' and po.product_id=' . $product_id;
        }
        $agenter_model->join('cmf_petsun_company pc', 'pc.id=a.company_id', 'left');
        $agenter_model->join('cmf_petsun_agenter_goods_sale gs', 'gs.agenter_id=a.id' . $gs_on_where, 'left');
        $agenter_model->join('cmf_petsun_product pp', 'pp.name=gs.goods_name and pp.delete_time=0', 'left');

        $agenter_model->group('a.id');
        $agenter_model->where($where);
        $agenter_buildSql = $agenter_model->buildSql();
        $agenter_model->table($agenter_buildSql . ' a')
            ->field('a.*,(IFNULL(sum(gs.sale_num),0)+a.num) num,(IFNULL(sum(IFNULL(pp.price,0)*gs.sale_num),0)+a.total_price) total_price')
            ->join('cmf_petsun_store s', 's.agenter_id=a.id and s.delete_time=0', 'left')
            ->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id ' . $gs_on_where, 'left')
            ->join('cmf_petsun_product pp', 'pp.name=gs.goods_name and pp.delete_time=0', 'left')
            ->group('a.id');


        $order_field = 'num';
        if ($sort_zd == 1) {
            $order_field = 'num';
        }
        if ($sort_zd == 2) {
            $order_field = 'song_num';
        }
        if ($sort_zd == 3) {
            $order_field = 'total_price';
        }
        if ($sort_zd != 0) {
            if ($sortsize == 1) {
                $agenter_model->order($order_field, 'desc');
            }
            if ($sortsize == 2) {
                $agenter_model->order($order_field, 'asc');
            }
        }
        $this->assign('sort_zd', $sort_zd);

        $data = $agenter_model->paginate(10);
        $data->appends($param);

        //查询产品
        $product_model = new ProductModel();
        $where = ['delete_time' => 0];
        $product_model->field('id,name');
        $product_model->where($where);
        $product_list = $product_model->select()->toArray();
        $this->assign('product_list', $product_list);
        $this->assign('product_id', $product_id);


        $this->assign('list', $data->items());
        $this->assign('user_type', $user_type);
        $this->assign('page', $data->render());
        $this->assign('timewee', $timewee);
        $this->assign('sortsize', $sortsize);
        $param ['type'] = 1;
        $this->assign('vanurl', url('datastat/statAgenter', $param));
        if (isset($type) && $type == 1) {
            return $this->fetch('stat_agentera');
        }
        return $this->fetch();
    }

    /*
     * 代理公司统计
     */
    public function statCompany()
    {
        $param = $this->request->param();
        $company_model = new CompanyModel();
        $type = isset($param['type']) ? $param['type'] : 0;
        $sortsize = isset($param['sortsize']) ? $param['sortsize'] : 0;
        $company_model->alias('c');
        if ((!empty($param['start_time']) && empty($param['end_time'])) || (empty($param['start_time']) && !empty($param['end_time']))) {
            $this->error('请选择正确的时间段');
        }
        $on_where = '';
        $gs_on_where = '';
        if (!empty($param['start_time']) && !empty($param['end_time'])) {
            $start_time = strtotime($param['start_time']);
            $end_time = strtotime($param['end_time']);
            $on_where = ' and po.created_at>=' . $start_time . ' and po.created_at<=' . $end_time;
            $gs_on_where = ' and gs.created_at>="' . $param['start_time'] . '" and gs.created_at<="' . $param['end_time'] . '"';
            $this->assign('start_time', $param['start_time']);
            $this->assign('end_time', $param['start_time']);
        }
        //添加产品条件
        $product_id = isset($param['product_id']) ? $param['product_id'] : 0;
        if ($product_id != 0) {
            $company_model->where(['pp.id' => $product_id]);
        }
        $company_model->field('c.*,IFNULL(sum(gs.sale_num),0) num,IFNULL(sum(IFNULL(pp.price,0)*gs.sale_num),0) total_price');
        $where = ['c.delete_time' => 0];
        $company_model->join('cmf_petsun_store s', 'c.id=s.company_id', 'left');
        $company_model->join('cmf_petsun_store_goods_sale gs', 'gs.store_id=s.id' . $gs_on_where, 'left');
        $company_model->join('cmf_petsun_product pp', 'pp.name=gs.goods_name and pp.delete_time=0', 'left');

        $company_model->group('c.id');

        $company_model->where($where);
        $company_buildSql = $company_model->buildSql();
        $company_model->table($company_buildSql . ' a')
            ->field('a.*,(IFNULL(sum(ags.sale_num),0)+a.num) num,(IFNULL(sum(IFNULL(pp.price,0)*ags.sale_num),0)+a.total_price) total_price,pp.name')
            ->join('cmf_petsun_agenter pa', 'pa.company_id=a.id', 'left')
            ->join('cmf_petsun_agenter_goods_sale ags', ' ags.agenter_id=pa.id', 'left')
            ->join('cmf_petsun_product pp', 'pp.name=ags.goods_name and pp.delete_time=0', 'left')
            ->group('a.id');
        $sort_zd = isset($param['sort_zd']) ? $param['sort_zd'] : 0;
        $order_field = 'num';
        if ($sort_zd == 1) {
            $order_field = 'num';
        }
        if ($sort_zd == 2) {
            $order_field = 'song_num';
        }
        if ($sort_zd == 3) {
            $order_field = 'total_price';
        }
        if ($sort_zd != 0) {
            if ($sortsize == 1) {
                $company_model->order($order_field, 'desc');
            }
            if ($sortsize == 2) {
                $company_model->order($order_field, 'asc');
            }
        }
        $this->assign('sort_zd', $sort_zd);
        $data = $company_model->paginate(10);
        //查询产品
        $product_model = new ProductModel();
        $where = ['delete_time' => 0];
        $product_model->field('id,name');
        $product_model->where($where);
        $product_list = $product_model->select()->toArray();
        $this->assign('product_list', $product_list);
        $this->assign('product_id', $product_id);


        $data->appends($param);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        $this->assign('sortsize', $sortsize);
        $param ['type'] = 1;
        $this->assign('vanurl', url('datastat/statCompany', $param));
        if (isset($type) && $type == 1) {
            return $this->fetch('stat_companya');
        }
        return $this->fetch();
    }

    /**
     * 设置全国排名
     */
    public function storesort()
    {
        $param = $this->request->param();
        if (request()->isPost()) {
            $sort = $param['post'];
            cache('sort', $sort);
            $this->success('修改成功', url('datastat/storesort'));
        }
        $sort = cache('sort');
        if (empty($sort)) {
            //a_rsort是全国排名，b_rsort是全省排名，c_rsort是全市排名
            $sort = [
                'a_sort' => 100,
                'b_sort' => 50,
                'c_sort' => 20,
            ];
            cache('sort', $sort);
        }
        $this->assign('sort', $sort);
        return $this->fetch();
    }

}