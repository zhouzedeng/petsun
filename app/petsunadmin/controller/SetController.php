<?php
namespace app\petsunadmin\controller;

use app\comm\model\SetModel;
use cmf\controller\AdminBaseController;

class SetController extends AdminBaseController
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @return mixed
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/27 16:58
     */
    public function index()
    {
        $setModel = new SetModel();
        $couponrule = cache('couponrule');
        $this->assign('couponrule', $couponrule);
        $this->assign( 'data1' , $setModel->getOneByParam(['id' => 1]));
        $this->assign( 'data2' , $setModel->getOneByParam(['id' => 2]));
        return $this->fetch();
    }

    /**
     * @return mixed
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/27 17:21
     */
    public function edit()
    {
        $setModel = new SetModel();
        $data = $this->request->param();
        $id_1 = $data['id_one'];
        $id_2 = $data['id_two'];
        $setModel->updateById(['value' => $id_1], 1);
        $setModel->updateById(['value' => $id_2], 2);
        return redirect('set/index');
    }
}