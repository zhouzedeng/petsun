<?php
/**
 * 产品管理
 */

namespace app\petsunadmin\controller;


use app\comm\model\AdModel;
use app\comm\model\ProductModel;
use app\petsunadmin\service\ProductService;
use cmf\controller\AdminBaseController;

class ProductController extends AdminBaseController
{
    protected $product_service;

    public function initialize()
    {
        parent::initialize();
        $this->product_service = new ProductService();
    }

    /**
     * 产品管理列表
     */
    public function index()
    {
        $param = $this->request->param();
        $data = $this->product_service->getProductList($param);
        $this->assign('list', $data->items());
        $this->assign('page', $data->render());
        return $this->fetch();
    }

    /**
     * 添加产品
     */
    public function add()
    {

        if (request()->isPost()) {
            $data = $this->request->param();
            $data = $data['post'];
            if(empty($data['name'])){
                $this->error('产品名称不能为空');
            }
            $product_model = new ProductModel();
            $product = $product_model->getOneByParam(['name'=>$data['name']]);
            if(!empty($product)){
                $this->error('产品名称已存在');
            }
            $res = $this->product_service->add($data);
            if ($res == true) {
                $this->success('添加成功', url('Product/index'));
            } else {
                $this->error('添加失败', url('Product/index'));
            }
        }
        return $this->fetch();
    }

    /**
     * 编辑产品
     */
    public function edit()
    {
        $data = $this->request->param();
        $product_model = new ProductModel();
        if ($this->request->isPost()) {
            $imgs = [];
            foreach ($data['post']['img_url'] as $v) {
                $id = $product_model->upImg($v);
                if (empty($id)) {
                    continue;
                }
                $imgs[] = $id;
            }
            $data['post']['img_url'] = implode(',', $imgs);
            $res = $product_model->allowField(true)->isUpdate(true)->data($data['post'], true)->save();
            $this->success('修改成功', url('Product/index'));
        } else {
            $product_model->alias('p');
            $product_model->join('cmf_petsun_imgs pi', 'FIND_IN_SET(pi.id, p.img_url)', 'left');
            $product_model->group('p.id');
            $product_model->field('p.*,group_concat(pi.img_name) as img_name');
            $info = $product_model->where(['p.id' => $data['id']])->find();

            $info ['img_name'] = empty($info ['img_name']) ? '' : explode(',', $info ['img_name']);
            $info ['img_url'] = explode(',', $info ['img_url']);
            if (empty($info ['img_name']) || count($info ['img_url']) != count($info ['img_name'])) {
                $info ['imgs'] = '';
            } else {
                $info ['imgs'] = array_combine($info ['img_url'], $info ['img_name']);
            }
            $this->assign('post', $info);
        }
        return $this->fetch();
    }

    /**
     * 删除产品
     */
    public function delete()
    {
        $data = $this->request->param();
        $product_model = new ProductModel();

        $result = $product_model->save(['delete_time' => time()], ['id' => $data['id']]);
        if ($result) {
            $this->success(lang('删除成功'));
        } else {
            $this->error(lang('删除失败'));
        }
    }
}