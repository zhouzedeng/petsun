<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 2019/3/1
 * Time: 22:54
 * 广告管理
 */

namespace app\petsunadmin\controller;


use app\comm\model\AdModel;
use app\petsunadmin\service\AdService;
use cmf\controller\AdminBaseController;

class AdController extends AdminBaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 广告图管理列表
     */
    public function adlist()
    {
        $param = $this->request->param();
        $ad_service = new AdService();
        $data = $ad_service->getAdList($param);
        $this->assign('list', $data->items());
        return $this->fetch();
    }

    /**
     * 添加广告
     */
    public function add()
    {

        if (request()->isPost()) {
            $data = $this->request->param();
            $ad_service = new AdService();
            $res = $ad_service->addad($data['post']);
            if ($res == true) {
                $this->success('添加成功', url('ad/adlist'));
            } else {
                $this->error('添加失败', url('ad/adlist'));
            }
        }
        return $this->fetch();
    }

    /**
     * 编辑广告
     */
    public function edit()
    {
        $data = $this->request->param();
        $admodel = new AdModel();
        if ($this->request->isPost()) {
            if (empty($data['post']['img_id'])) {
                $admodel->upImg($data['post']['img_url'], $data['post']['img_id']);
            } else {
                $img_id = $admodel->upImg($data['post']['img_url']);
                $data['post']['img_url'] = $img_id;
            }
            $res = $admodel->allowField(true)->isUpdate(true)->data($data['post'], true)->save();
            $this->success('修改成功', url('ad/adlist'));
        } else {
            $admodel->alias('a');
            $admodel->join('cmf_petsun_imgs pi', 'pi.id=a.img_url', 'left');
            $admodel->field('a.*,pi.img_name');
            $info = $admodel->where(['a.id' => $data['id']])->find();
            $this->assign('post', $info);
        }
        return $this->fetch();
    }

    /**
     * 删除广告
     */
    public function delete()
    {
        $data = $this->request->param();
        $admodel = new AdModel();
        $result = $admodel->where(['id' => $data['id']])->delete();
        if ($result) {
            $this->success(lang('删除成功'));
        } else {
            $this->error(lang('删除失败'));
        }
    }
}