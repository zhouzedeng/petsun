<?php
namespace app\openapi\controller;

use app\openapi\service\GoodsService;

/**
 * Class GoodsController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/3/11 15:02
 */
class GoodsController extends BaseController
{
    private $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new GoodsService();
    }

    /**
     * 获取医院下的商品列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/12 10:28
     */
    public function getStoreGoodsList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('400000', 'token不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('400002', 'store_id不能为空');
        }
        $data = $this->service->getStoreGoodsList($params);
        apiSuccess($data);
    }

    /**
     * 获取优惠券列表
     * @Author Zed
     * @Time 2019/3/12 10:55
     */
    public function getCouponList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('400000', 'token不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('400000', 'store_id不能为空');
        }
        $data = $this->service->getCouponList($params);
        apiSuccess($data);
    }

    /**
     * 医院人员扫描商品
     * @Author Zed
     * @Time 2019/3/12 16:17
     *
     */
    public function placeOrderWhenScanEnd2C()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('400000', 'token不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('400002', 'store_id不能为空');
        }
        if (empty($params['goods_serial'])) {
            apiFail('400002', 'goods_serial不能为空');
        }
        if (empty($params['address'])) {
            apiFail('400002', 'address不能为空');
        }
        if (empty($params['scan_id'])) {
            apiFail('400002', 'scan_id不能为空');
        }
        $data = $this->service->placeOrderWhenScanEnd2C($params);
        apiLog($params, $data);
        apiSuccess($data);
    }

    /**
     * 代理商扫描商品
     * @Author Zed
     * @Time 2019/3/12 16:17
     *
     */
    public function placeOrderWhenScanEnd2B()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('400000', 'token不能为空');
        }
        if (empty($params['goods_serial'])) {
            apiFail('400002', 'goods_serial不能为空');
        }
        if (empty($params['address'])) {
            apiFail('400002', 'address不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('400002', 'store_id不能为空');
        }
        if (empty($params['scan_id'])) {
            apiFail('400002', 'scan_id不能为空');
        }
        $data = $this->service->placeOrderWhenScanEnd2B($params);
        apiLog($params, $data);
        apiSuccess($data);
    }
}