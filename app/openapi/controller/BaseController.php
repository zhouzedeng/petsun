<?php
namespace app\openapi\controller;

/**
 * Class BaseController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/3/1 9:13
 */
class BaseController
{
    protected $params = null;

    public function __construct()
    {
        $this->params = array_merge($_GET, $_POST);
    }
}