<?php
namespace app\openapi\controller;

use app\comm\lib\SignatureHelper;
use app\comm\model\ImgsModel;
use app\openapi\service\CommonService;
use think\facade\Cache;
use think\log;

/**
 * Class UserController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/3/1 10:11
 */
class CommonController extends BaseController
{
    protected $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new CommonService();
    }

    /**
     * 获取广告列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function getAdList()
    {
        $data = $this->service->getAdList();
        apiSuccess($data);
    }

    /**
     * 获取省份列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function getProvinceList()
    {
        $data = $this->service->getProvinceList();
        apiSuccess($data);
    }

    /**
     * 获取城市列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function getCityList()
    {
        $params = $this->params;
        if (empty($params['province_id'])) {
            apiFail('100003', '省份ID不能为空');
        }
        $data = $this->service->getCityList($params);
        apiSuccess($data);
    }

    /**
     * 获取地区列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function getAreaList()
    {
        $params = $this->params;
        if (empty($params['city_id'])) {
            apiFail('100003', '城市ID不能为空');
        }
        $data = $this->service->getAreaList($params);
        apiSuccess($data);
    }

    /**
     * 上传图片接口
     * @Author Zed
     * @Time 2019/3/11 14:17
     */
    public function upload()
    {
        $imgsModel = new ImgsModel();

        $file = request()->file('file');
        if(empty($file)) {
            apiFail('700001', '请选择上传文件');
        }
        $info = $file->move(ROOT_PATH.'public'.DS.'upload');
        $filename = $info->getSaveName();  //在测试的时候也可以直接打印文件名称来查看
        if ($filename) {
            $addData = [
                'img_name' => $filename,
                'created_at' => time()
            ];
            $id = $imgsModel->add($addData);
            apiSuccess(['img_id' => $id, 'img_url'=>cmf_get_image_url($filename)]);
        } else {
            // 上传失败获取错误信息
            apiFail('700002', $file->getError());
        }
    }

    /**
     * @param $to
     * @param string $code
     * @return bool
     * @throws ClientException
     * @Author Zed
     * @Time 2019/3/14 11:23
     */
    public function sendSms()
    {
        $params = $this->params;
        if (empty($params['phone'])) {
            apiFail('100000', '电话不能为空');
        }
        $code = rand(100000, 999999);
        $param = array ();

        // fixme 必填：是否启用https
        $security = false;

        // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
        $accessKeyId = "LTAIGwbXrDi67oMj";
        $accessKeySecret = "xokns23ALwRrQL7Aby53JrxcE44SWA";

        // fixme 必填: 短信接收号码
        $param["PhoneNumbers"] = $params['phone'];

        // fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
        $param["SignName"] = "沛生医药";

        // fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
        $param["TemplateCode"] = "SMS_160015110";

        // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
        $param['TemplateParam'] = Array (
            "code" => $code,
        );

        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        if(!empty($param["TemplateParam"]) && is_array($param["TemplateParam"])) {
            $param["TemplateParam"] = json_encode($param["TemplateParam"], JSON_UNESCAPED_UNICODE);
        }

        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();

        // 此处可能会抛出异常，注意catch
        $content = $helper->request(
            $accessKeyId,
            $accessKeySecret,
            "dysmsapi.aliyuncs.com",
            array_merge($param, array(
                "RegionId" => "cn-hangzhou",
                "Action" => "SendSms",
                "Version" => "2017-05-25",
            )),
            $security
        );
        $content = obj2arr($content);
        if ($content['Message'] !== 'OK') {
            log::write($content,'info');
            apiFail('100001', '发送失败');
        }
        Cache::store('default')->set($params['phone'], $code, 5 * 60);
        apiSuccess();
    }


    /**
     * @Author Zed
     * @Time 2019/4/1 11:30
     */
    public function getAppConfig()
    {
        $data = cache('appconfig');
        $data['xcx_img'] = cmf_get_image_url($data['xcx_img']);
        apiSuccess($data);
    }

    /**
     * @Author Zed
     * @Time 2019/4/1 11:30
     */
    public function getSort()
    {
        $data = cache('sort');
        apiSuccess($data);
    }
}