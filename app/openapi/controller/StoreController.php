<?php

namespace app\openapi\controller;

use app\comm\model\ClerkModel;
use app\comm\model\StoreModel;
use app\openapi\service\StoreService;

/**
 * Class UserController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/2/28 11:32
 */
class StoreController extends BaseController
{
    private $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new StoreService();
    }

    /**
     * 医院注册
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 9:43
     */
    public function register()
    {
        $params = $this->params;
        if (empty($params['contact_name']) || empty($params['contact_phone'])) {
            apiFail('20000', '用户名称和手机号不能为空');
        }
        if (empty($params['store_name']) || empty($params['address_detail'])) {
            apiFail('20001', '医院名称和地址不能为空');
        }
        /*  if (empty($params['province']) || empty($params['city']) || empty($params['area'])) {
              apiFail('20002', '省市区名称不能为空');
          }*/
        if (empty($params['store_img_ids'])) {
            apiFail('20002', '医院图片ID不能为空');
        }
        if (empty($params['lon']) || empty($params['lat'])) {
            apiFail('20000', '经度和纬度不能为空');
        }
        $info = $this->service->verifyAddress($params);
        if ($info) {
            apiFail('20001', '该医院已经注册');
        }
        $this->service->register($params);
        apiSuccess();
    }

    /**
     * 获取门店详情
     * @Author Zed
     * @Time 2019/3/4 10:52
     */
    public function getStoreInfo()
    {
        $params = $this->params;
        if (empty($params['store_id'])) {
            apiFail('20003', '医院ID不能为空');
        }
        if (empty($params['token'])) {
            apiFail('20003', 'token不能为空');
        }
        $data = $this->service->getStoreInfo($params);
        apiSuccess($data);
    }


    /**
     * 获取特色列表
     * @Author Zed
     * @Time 2019/3/6 11:03
     */
    public function getTsList()
    {
        $data = $this->service->getTsList();
        apiSuccess($data);
    }

    /**
     * 代理新增医院
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 9:43
     */
    public function createStore()
    {
        $params = $this->params;
        if (empty($params['lon']) || empty($params['lat'])) {
            apiFail('20000', '经度和纬度不能为空');
        }
        if (empty($params['name']) || empty($params['address_detail'])) {
            apiFail('20001', '医院名称和地址不能为空');
        }
        if (empty($params['province']) || empty($params['city'])) {
            apiFail('20002', '省市名称不能为空');
        }
        if (empty($params['ts'])) {
            apiFail('20002', '特色不能为空');
        }
        if (empty($params['clerk_name'])) {
            apiFail('20002', '店长姓名不能为空');
        }
        if (empty($params['clerk_phone'])) {
            apiFail('20002', '店长电话不能为空');
        }
        if (empty($params['store_imgs'])) {
            apiFail('20002', 'store_imgs不能为空');
        }
        $info = $this->service->verifyAddress($params);
        if ($info) {
            apiFail('20001', '该医院已经注册');
        }
        $data = $this->service->createStore($params);
        apiSuccess($data);
    }

    /**
     * 删除医院
     */
    public function deleteStore()
    {
        $params = $this->params;
        if (empty($params['store_id'])) {
            apiFail('20002', '门店ID不能为空');
        }
        if (empty($params['token'])) {
            apiFail('20002', 'token不能为空');
        }
        $clerk = $this->service->getClerkFromRedis($params['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }

        $params = $this->params;
        $store_model = new StoreModel();
        $clerk_model = new ClerkModel();
        $store_model->startTrans();
        $store_res = $store_model->save(['delete_time' => time()], ['id' => $params['store_id']]);
        $clerk_res = $clerk_model->save(['delete_time' => time()], ['store_id' => $params['store_id']]);
        if ($store_res && $clerk_res) {
            $store_model->commit();
            apiSuccess();
        } else {
            $store_model->rollback();
            apiFail('10000');
        }
    }

    /**
     * 代理编辑医院
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 9:43
     */
    public function editStore()
    {
        $params = $this->params;
        if (empty($params['lon']) || empty($params['lat'])) {
            apiFail('20000', '经度和纬度不能为空');
        }
        if (empty($params['name']) || empty($params['address_detail'])) {
            apiFail('20001', '医院名称和地址不能为空');
        }
        if (empty($params['province']) || empty($params['city'])) {
            apiFail('20002', '省市名称不能为空');
        }
        if (empty($params['ts'])) {
            apiFail('20002', '特色不能为空');
        }
        if (empty($params['clerk_name'])) {
            apiFail('20002', '店长姓名不能为空');
        }
        if (empty($params['clerk_phone'])) {
            apiFail('20002', '店长电话不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('20002', '门店ID不能为空');
        }
        if (empty($params['clerk_id'])) {
            apiFail('20002', '店长ID不能为空');
        }
        $this->service->editStore($params);
        apiSuccess();
    }

    /**
     * 获取医院排名
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/12 10:19
     */
    public function getStoreRank()
    {
        $params = $this->params;
        if (empty($params['store_id'])) {
            apiFail('20002', '门店ID不能为空');
        }
        if (empty($params['token'])) {
            apiFail('20002', 'token不能为空');
        }
        $data = $this->service->getStoreRank($params);
        apiSuccess($data);
    }
}