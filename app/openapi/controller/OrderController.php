<?php
namespace app\openapi\controller;

use app\openapi\service\OrderService;

/**
 * Class ProductController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/2/28 11:32
 */
class OrderController extends BaseController
{
    private $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new OrderService();
    }

    /**
     * 下单
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 9:43
     */
    public function placeOrder()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('400000', 'token不能为空');
        }
        if (empty($params['product_id'])) {
            apiFail('400001', 'product_id不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('400001', 'store_id不能为空');
        }
        if (empty($params['number'])) {
            apiFail('400001', 'number不能为空');
        }
        $data = $this->service->placeOrder($params);
        apiSuccess($data);
    }
}