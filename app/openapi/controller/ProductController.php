<?php
namespace app\openapi\controller;

use app\openapi\service\ProductService;

/**
 * Class ProductController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/2/28 11:32
 */
class ProductController extends BaseController
{
    private $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new ProductService();
    }

    /**
     * 获取产品列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 9:43
     */
    public function getProductList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('300000', 'token不能为空');
        }
        $data = $this->service->getProductList($params);
        apiSuccess($data);
    }

    /**
     * 获取产品列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 9:43
     */
    public function getProductInfo()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('300000', 'token不能为空');
        }
        if (empty($params['product_id'])) {
            apiFail('300001', 'product_id不能为空');
        }
        $data = $this->service->getProductInfo($params);
        apiSuccess($data);
    }
}