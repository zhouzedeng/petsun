<?php
namespace app\openapi\controller;

use app\openapi\service\AgenterService;

/**
 * Class UserController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/2/28 11:32
 */
class AgenterController extends BaseController
{
    private $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new AgenterService();
    }

    /**
     * 获取代理的医院列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function getAgenterStoreList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('10002', 'token不能为空');
        }
        $data = $this->service->getAgenterStoreList($params);
        apiSuccess($data);
    }

    /**
     * 获取门店详情
     * @Author Zed
     * @Time 2019/3/4 10:52
     */
    public function getStoreInfo()
    {
        $params = $this->params;
        if (empty($params['store_id'])) {
            apiFail('20003', '医院ID不能为空');
        }
        if (empty($params['token'])) {
            apiFail('20003', 'token不能为空');
        }
        $data = $this->service->getStoreInfo($params);
        apiSuccess($data);
    }

    /**
     * 获取通知列表
     * @Author Zed
     * @Time 2019/3/6 15:24
     */
    public function getNoticeList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('20003', 'token不能为空');
        }
        $data = $this->service->getNoticeList($params);
        apiSuccess($data);
    }

    /**
     * 落实预订单
     * @Author Zed
     * @Time 2019/3/7 16:33
     */
    public function FinishOrder()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('20003', 'token不能为空');
        }
        if (empty($params['order_id'])) {
            apiFail('20003', 'order_id不能为空');
        }
        $data = $this->service->FinishOrder($params);
        apiSuccess($data);
    }

    /**
     * 数据统计
     * @Author Zed
     * @Time 2019/3/7 16:33
     */
    public function getDataList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('20003', 'token不能为空');
        }
        if (empty($params['goods_id'])) {
            apiFail('20003', 'goods_id不能为空');
        }
        if (empty($params['year'])) {
            apiFail('20003', 'year不能为空');
        }
        if (empty($params['month_type'])) {
            apiFail('20003', 'month_type不能为空');
        }
        if (empty($params['role_type'])) {
            apiFail('20003', 'role_type不能为空');
        }

        $data = $this->service->getDataList($params);
        apiSuccess($data);
    }

    /**
     * @Author Zed
     * @Time 2019/3/19 10:42
     */
    public function getAgenterInfo()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('20003', 'token不能为空');
        }
        $data = $this->service->getAgenterInfo($params);
        apiSuccess($data);
    }
}