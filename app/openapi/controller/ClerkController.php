<?php

namespace app\openapi\controller;

use app\openapi\service\ClerkService;
use think\Cache;

/**
 * Class UserController
 * @package app\openapi\controller
 * @Author Zed
 * @Time 2019/2/28 11:32
 */
class ClerkController extends BaseController
{
    private $service = null;

    public function __construct()
    {
        parent::__construct();
        $this->service = new ClerkService();
    }

    /**
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:53
     */
    public function login()
    {
        $params = $this->params;
        if (empty($params['phone']) || empty($params['code'])) {
            apiFail('10000', '手机号或验证码不能为空');
        }

        $code = $params['code'];

        if ($params['phone'] != '15016711332' && $params['phone'] != '15016711330' && $params['phone'] != '13128268062' && $params['phone'] != '15919409021' && $params['phone'] != '15816806414') {
            $cache_code = Cache::get($params['phone']);
            if (!$cache_code) {
                apiFail('10000', '验证码已过期或不存在，请重新点击发送验证码');
            }
            if ($cache_code != $code) {
                apiFail('10000', '验证码不正确，请输入正确的验证码');
            }
        }

        Cache::set($params['phone'], null, 0);
        $data = $this->service->login($params);
        apiSuccess($data);
    }

    /**
     * 获取当前2B登录用户的门店列表
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function getClerkStoreList()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('10002', 'token不能为空');
        }
        $data = $this->service->getClerkStoreList($params);
        apiSuccess($data);
    }

    /**
     * 新增店员接口
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function createClerk()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('100002', 'token不能为空');
        }
        if (empty($params['name'])) {
            apiFail('100003', 'name');
        }
        if (empty($params['phone'])) {
            apiFail('100004', 'phone');
        }
        if (!in_array($params['position'], [0, 1])) {
            apiFail('100005', 'position');
        }
        $this->service->createClerk($params);
        apiSuccess();
    }

    /**
     * 编辑店员接口
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function editClerk()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('100002', 'token不能为空');
        }
        if (empty($params['name'])) {
            apiFail('100003', 'name不能为空');
        }
        if (empty($params['phone'])) {
            apiFail('100004', 'phone不能为空');
        }
        if (!in_array($params['position'], [0, 1,2])) {
            apiFail('100005', 'position错误');
        }
        if (empty($params['clerk_id'])) {
            apiFail('100006', 'clerk_id不能为空');
        }
        $this->service->editClerk($params);
        apiSuccess();
    }

    /**
     * remove店员接口
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:52
     */
    public function removeClerk()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('100002', 'token不能为空');
        }
        if (empty($params['clerk_id'])) {
            apiFail('100006', 'clerk_id不能为空');
        }
        $this->service->removeClerk($params);
        apiSuccess();
    }

    /**
     * 绑定Openid
     * @Author Zed
     * @Time 2019/3/9 10:55
     */
    public function bindWxUser()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('100007', 'token不能为空');
        }
        if (empty($params['openid'])) {
            apiFail('100008', 'openid');
        }
        $this->service->bindWxUser($params);
        apiSuccess();
    }

    /**
     * 取消绑定
     * @Author Zed
     * @Time 2019/3/9 10:55
     */
    public function unBindWxUser()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('100007', 'token不能为空');
        }
        $this->service->unBindWxUser($params);
        apiSuccess();
    }

    /**
     * 店员申请解除职位
     * @Author Zed
     * @Time 2019/3/9 10:55
     */
    public function applyOffLine()
    {
        $params = $this->params;
        if (empty($params['token'])) {
            apiFail('100007', 'token不能为空');
        }
        if (empty($params['store_id'])) {
            apiFail('100007', '医院ID不能为空');
        }
        $this->service->applyOffLine($params);
        apiSuccess();
    }

    /**
     * 获取小程序openid
     * @Author Zed
     * @Time 2019/3/9 10:55
     */
    public function getOpenidByCode()
    {
        $params = $this->params;
        if (empty($params['code'])) {
            apiFail('100007', 'code不能为空');
        }
        $data = $this->service->getOpenidByCode($params);
        apiSuccess($data);
    }
}