<?php
namespace app\openapi\service;

use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\OpenidPhoneMapModel;
use app\comm\model\StoreModel;
use app\comm\model\UnbindLogModel;
use think\Cache;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class ClerkService extends BaseService
{
    private $clerkModel= null;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->clerkModel = new ClerkModel();
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 9:31
     */
    public function login($data = array())
    {
        $agenterModel = new AgenterModel();
        $openidPhoneMapModel = new OpenidPhoneMapModel();

        $param = [];
        $param['phone'] = $data['phone'];
        $param['delete_time'] = 0;
        $userInfo = $this->clerkModel->getOneByParam($param);
        $agenterInfo = $agenterModel->getOneByParam($param);
        if (!$userInfo && !$agenterInfo) {
            apiFail('10001', '该手机号尚未注册');
        }
        if ($agenterInfo) {
            $agenterModel->updateById(['openid' => $data['openid']] ,$agenterInfo['id']);
        } else {
            $this->clerkModel->updateById(['openid' => $data['openid']] ,$userInfo['id']);
        }

        $role = $agenterInfo ? 1 : 2;  // 1 : 代理端, 2 : 医生端
        $token = getToken();
        $phone  = $role == 1 ? $agenterInfo['phone'] : $userInfo['phone'];
        $openid = $data['openid'];
        $cache_info = array('phone' => $phone, 'openid' => $openid);
        Cache::store('default')->set(getPetSunUserPrefixKey() . $token, $cache_info, 7 * 86400);

        $param = [];
        $param['phone'] = $phone;
        $info = $openidPhoneMapModel->getOneByParam($param);
        if (!$info) {
            $addData = [];
            $addData['phone'] = $phone;
            $addData['openid'] = $openid;
            $openidPhoneMapModel->add($addData);
        }

        return ['token' => $token, 'role' => $role];
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function getClerkStoreList($data = array())
    {
        $storeModel = new StoreModel();
        $storeService = new StoreService();
        $commonService = new CommonService();
        $agenterModel = new AgenterModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone= $clerk['phone'];

        $param = [];
        $param['phone'] = $phone;
        $param['is_use'] = 1;
        $param['delete_time'] = 0;
        $clerkList = $this->clerkModel->getListByParam($param);
        if (empty($clerkList)) {
            apiFail('10001', '该手机号还没有关联有医院');
        }
        $storeIds = array_column(obj2arr($clerkList), 'store_id');
        $storeIdToNmae = array_column(obj2arr($clerkList), 'name', 'store_id');
        $storeIdToPosition = array_column(obj2arr($clerkList), 'position', 'store_id');


        $param = [];
        $param['id'] = ['in', $storeIds];
        $param['check_status'] = 1; // 审核通过
        $param['delete_time'] = 0; // 审核通过
        $storeList = $storeModel->getListByParam($param);
        $storeList = obj2arr($storeList);

        // 代理商
        $agenter_ids = array_column($storeList, 'agenter_id');
        $param = [];
        $param['id'] = ['in', $agenter_ids];
        $agenterList = $agenterModel->getListByParam($param);
        $agenterList = obj2arr($agenterList);
        $agenterTmpList = [];
        foreach ($agenterList as $k => $v) {
            $agenterTmpList[$v['id']] = $v;
        }

        foreach ($storeList as $k => $v) {
            $storeList[$k]['clerkName'] = $storeIdToNmae[$v['id']];
            $storeList[$k]['position'] = $storeIdToPosition[$v['id']];
            $storeList[$k]['positionName'] = getPositionById($storeIdToPosition[$v['id']]);
            $storeList[$k]['phone'] = $clerk['phone'];
            $storeList[$k]['province_name'] = $storeService->getRegionNameById($v['province_id']);
            $storeList[$k]['city_name'] = $storeService->getRegionNameById($v['city_id']);
            $storeList[$k]['area_name'] = $storeService->getRegionNameById($v['area_id']);
            $storeList[$k]['store_imgs'] = empty($v['store_imgs']) ? [] : $commonService->getImgFullUrlFormat(explode(',', $v['store_imgs']));
            $storeList[$k]['agenter_name'] = isset($agenterTmpList[$v['agenter_id']]) ? $agenterTmpList[$v['agenter_id']]['name'] : '';
            $storeList[$k]['agenter_phone'] = isset($agenterTmpList[$v['agenter_id']]) ? $agenterTmpList[$v['agenter_id']]['phone'] : '';

        }
        return $storeList;
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function createClerk($data = array())
    {
        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }

        // 手机号不能是已存在在代理人表中
        $agenterModel = new AgenterModel();
        $param = [];
        $param['phone'] = $data['phone'];
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if ($agenter) {
            apiFail('100006', '该手机号是代理人，不能加入店员身份');
        }

        // 查重
        $param = [];
        $param['phone'] = $data['phone'];
        $param['store_id'] = $data['store_id'];
        $param['delete_time'] = 0;
        $clerk = $this->clerkModel->getOneByParam($param);
        if ($clerk) {
            apiFail('100006', '该手机号已存在当前医院中，不能重复添加');
        }

        $addDtata = [
            'store_id' => $data['store_id'],
            'name' => $data['name'],
            'phone' => $data['phone'],
            'position' => $data['position'],
            'delete_time' => 0,
            'created_at' => time(),
            'openid' => ''
        ];
        $this->clerkModel->add($addDtata);
    }


    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function removeClerk($data = array())
    {
        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $editDtata = [
            'delete_time' => time(),
        ];
        $this->clerkModel->updateById($editDtata, $data['clerk_id']);
    }
    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function editClerk($data = array())
    {
        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }

        // 手机号不能是已存在在代理人表中
        $agenterModel = new AgenterModel();
        $param = [];
        $param['phone'] = $data['phone'];
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if ($agenter) {
            apiFail('100006', '该手机号是代理人，不能加入店员身份');
        }

        $param = [];
        $param['id'] = $data['clerk_id'];
        $param['delete_time'] = 0;
        $clerk = $this->clerkModel->getOneByParam($param);
        if (!$clerk) {
            apiFail('100007', 'clerk_id错误');
        }

        $param = [];
        $param['store_id'] = $clerk['store_id'];
        $param['phone'] = $data['phone'];
        $param['id'] = ['not in', [$data['clerk_id']]];
        $param['delete_time'] = 0;
        $clerk = $this->clerkModel->getOneByParam($param);
        if ($clerk) {
            apiFail('100006', '该手机号已存在当前医院中，不能重复');
        }

        $editDtata = [
            'name' => $data['name'],
            'phone' => $data['phone'],
            'position' => $data['position']
        ];
        $this->clerkModel->updateById($editDtata, $data['clerk_id']);
    }

    /**
     * @param array $data
     * @return bool
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/9 11:15
     */
    public function bindWxUser($data = [])
    {
        $openidPhoneMapModel = new OpenidPhoneMapModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone= $clerk['phone'];

        $param = [];
        $param['phone'] = $phone;
        $info = $openidPhoneMapModel->getOneByParam($param);
        if ($info) {
            return false;
        }

        $addData = [];
        $addData['phone'] = $phone;
        $addData['openid'] = $data['openid'];
        $openidPhoneMapModel->add($addData);
    }

    /**
     * @param array $data
     * @return bool
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/9 11:15
     */
    public function unBindWxUser($data = [])
    {
        $openidPhoneMapModel = new OpenidPhoneMapModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone= $clerk['phone'];

        $param = [];
        $param['phone'] = $phone;
        $info = $openidPhoneMapModel->getOneByParam($param);
        if (!$info) {
            Cache::store('default')->set(getPetSunUserPrefixKey() . $data['token'], null, -1);
            return false;
        }
        $openidPhoneMapModel->delById($info['id']);
        Cache::store('default')->set(getPetSunUserPrefixKey() . $data['token'], null, -1);
    }

    /**
     * @param array $data
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/9 11:50
     */
    public function applyOffLine($data = [])
    {
        $unbindLogModel = new UnbindLogModel();
        $storeModel = new StoreModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone= $clerk['phone'];

        $param = [];
        $param['phone'] = $phone;
        $param['store_id'] = $data['store_id'];
        $param['delete_time'] = 0;
        $clerkInfo = $this->clerkModel->getOneByParam($param);
        if (empty($clerkInfo)) {
            apiFail('10001', '该手机号还没有关联有医院');
        }

        $param = [];
        $param['id'] =  $data['store_id'];
        $store = $storeModel->getOneByParam($param);
        if (empty($store)) {
            apiFail('10001', '医院Id error');
        }

        $addData = [
            'unbind_name' => $clerkInfo['name'],
            'unbind_phone' => $phone,
            'store_id' => $clerkInfo['store_id'],
            'status' => 1,
            'old_agenter_id' => $store['agenter_id'],
            'created_at' => time(),
            'no_use_desc' => empty($data['no_use_desc']) ? '' : $data['no_use_desc'],
            'no_use_imgs' => empty($data['no_use_imgs']) ? '' : $data['no_use_imgs'],
        ];
        $unbindLogModel->add($addData);
    }

    /**
     * @param array $data
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/9 14:37
     */
    public function getOpenidByCode($data = [])
    {
        $result = ['is_login' => 0, 'openid' => '', 'token' => '', 'role' => ''];
        $openidPhoneMapModel = new OpenidPhoneMapModel();

        $agenterModel = new AgenterModel();
        $openidPhoneModel = new OpenidPhoneMapModel();

        $appid = 'wx30e16dbfbad84a7d';
        $secret = '4cbb37a28aaec7c11e6904457f4fede8';
        if (empty($data['code'])) {
            apiFail('100005', 'code不能为空');
        }
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid={$appid}&secret={$secret}&js_code={$data['code']}&grant_type=authorization_code";
        $data = getByCurl($url);
        if ($data === false) {
            apiFail('100008', '请求微信URL失败，返回false');
        }
        $data = json_decode($data, true);

        if (!isset($data['openid'])) {
            apiFail('100008', $data['errmsg']);
        }
        $openid = $data['openid'];
        $result['openid'] = $openid;

        $param = [];
        $param['openid'] = $openid;
        $map = $openidPhoneModel->getOneByParam($param);
        if(!$map) {
            return $result;
        }
        $phone = $map['phone'];

        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $userInfo = $this->clerkModel->getOneByParam($param);
        $agenterInfo = $agenterModel->getOneByParam($param);
        if (!$userInfo && !$agenterInfo) {
            apiFail('10001', '该手机号尚未注册');
        }
        if ($agenterInfo) {
            $agenterModel->updateById(['openid' => $openid] ,$agenterInfo['id']);
        } else {
            $this->clerkModel->updateById(['openid' => $openid] ,$userInfo['id']);
        }

        $role = $agenterInfo ? 1 : 2;  // 1 : 代理端, 2 : 医生端
        $token = getToken();
        $phone  = $role == 1 ? $agenterInfo['phone'] : $userInfo['phone'];
        $cache_info = array('phone' => $phone, 'openid' => $openid);
        Cache::store('default')->set(getPetSunUserPrefixKey() . $token, $cache_info, 7 * 86400);

        $param = [];
        $param['phone'] = $phone;
        $info = $openidPhoneMapModel->getOneByParam($param);
        if (!$info) {
            $addData = [];
            $addData['phone'] = $phone;
            $addData['openid'] = $openid;
            $openidPhoneMapModel->add($addData);
        }

        $result['token'] = $token;
        $result['is_login'] = 1;
        $result['role'] = $role;
        apiSuccess($result);
    }
}
