<?php

namespace app\openapi\service;

use app\comm\model\AgenterGoodsSaleModel;
use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\CouponsModel;
use app\comm\model\GoodsModel;
use app\comm\model\ProductModel;
use app\comm\model\SetModel;
use app\comm\model\StoreGoodsSaleModel;
use app\comm\model\StoreModel;
use think\Db;
use think\Exception;

/**
 * 商品
 * Class GoodsService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/3/12 10:28
 */
class GoodsService extends BaseService
{
    private $model = null;

    /**
     * GoodsService constructor.
     */
    public function __construct()
    {
        $this->model = new GoodsModel();
    }

    /**
     * 获取医院下的商品列表
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/11 15:22
     */
    public function getStoreGoodsList($data = [])
    {
        $param = [];
        $param['store_id'] = $data['store_id'];
        $list = $this->model->getListByParam($param);
        $list = obj2arr($list);
        $tmp = [];
        foreach ($list as $k => $v) {
            if (isset($tmp[$v['goods_name_code']])) {
                unset($list[$k]);
            }
            $tmp[$v['goods_name_code']] = 1;
        }
        $list = array_values($list);
        return $list;
    }

    /**
     * @param array $data
     * @return array
     * @throws exception
     * @Author Zed
     * @Time 2019/3/14 14:41
     */
    public function getCouponList($data = [])
    {
        $couponsModel = new CouponsModel();

        $param = [];
        $param['store_id'] = $data['store_id'];
        !empty($data['status']) && $param['status'] = $data['status'] - 1;
        $order = ['status' => 'asc'];
        $list = $couponsModel->getListByParam($param, null, null, null, $order);
        $list = obj2arr($list);
        $goods_id = array_column($list, 'goods_id');

        // 商品
        $param = [];
        $param['id'] = ['in', $goods_id];
        $goods_list = $this->model->getListByParam($param);
        $goods_list = obj2arr($goods_list);
        $goods_id_name_list = array_column($goods_list, 'goods_name', 'id');

        $couponList = cache('couponrule');
        foreach ($list as $k => $v) {
            $list[$k]['goods_name'] = isset($goods_id_name_list[$v['goods_id']]) ? $goods_id_name_list[$v['goods_id']] : '';
            $list[$k]['desc'] = '本次优惠以医院为单位';
            $list[$k]['type'] = '买' . $v['buy'] . '赠' . $v['give'];
            $list[$k]['status'] = $v['status'] == 1 ? '已使用' : '未使用';
            $list[$k]['dateValid'] = 0;
            if (time() > $v['end_time']) {
                $list[$k]['dateValid'] = 1;
            }
            $list[$k]['end_time'] = date('Y-m-d H:i:s', $v['end_time']);
            $list[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time']);

        }

        return $list;
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/12 16:28
     */
    public function placeOrderWhenScanEnd2B($data = [])
    {
        $agenterModel = new AgenterModel();
        $productModel = new ProductModel();
        $commonService = new CommonService();

        $goods_serial = $data['goods_serial'];
        // 检查token
        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('100003', 'token错误或已过期');
        }
        $phone = $cache_clerk['phone'];

        // 获取当前登录者ID
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $userInfo = $agenterModel->getOneByParam($param);
        if (!$userInfo) {
            apiFail('400005', '找不到该代理人信息');
        }

        // 获取商品信息
        $param = [];
        $param['goods_serial'] = $data['goods_serial'];
        $goods = $this->model->getOneByParam($param);
        if (empty($goods)) {
            apiFail('100000', '后台尚未拥有该二维码');
        }
        if ($goods['useful'] == 0) {
            apiFail('100002', '无效二维码');
        }

        //
        $param = [];
        $param['name'] = $goods['goods_name'];
        $product = $productModel->getOneByParam($param);
        $img_url = '';
        if ($product) {
            $imgs = $commonService->getImgFullUrlFormat($product['img_url']);
            $img_url = $imgs ? $imgs[0]['url'] : '';
        }

        if ($goods['scan_status'] == 1) {
            $return_result = ['coupon_list' => [], 'scan_status' => 2, 'img_url' => $img_url, 'sale_num' => 0, 'give_num' => 0, 'total' => $goods['goods_num'], 'goods_name' => $goods['goods_name']];
            return $return_result;
        }

        // 开始事务
        Db::startTrans();
        try {
            $goods_num = $goods['goods_num'];
            $return_result = $this->getSaleInfo2B($userInfo['id'], $goods['goods_name_code'], $goods_num, $data['store_id'], $data['scan_id']);
            $return_result['goods_name'] = $goods['goods_name'];
            $return_result['scan_status'] = 1;
            $return_result['img_url'] = $img_url;
            $editData = [
                'scan_status' => 1,
                'scan_time' => time(),
                'scan_address' => $data['address'],
                'clerk_id' => 0,
                'store_id' => $data['store_id'],
                'agenter_id' => $userInfo['id'],
                'company_id' => $userInfo['company_id'],
                'created_at' => time(),
            ];
            $this->model->updateById($editData, $goods['id']);

            if ($goods['type'] == 1) {
                $pid_code = $goods['goods_parent_serial'];
                $param = [];
                $param['goods_serial'] = $pid_code;
                $pid_record = $this->model->getOneByParam($param);
                $ppid_code = $pid_record ? $pid_record['goods_parent_serial'] : '';
                $param = [];
                $param['goods_serial'] = $ppid_code;
                $ppid_record = $this->model->getOneByParam($param);
                $pid_num = $pid_record['goods_num'] - 1;
                $ppid_num = $ppid_record['goods_num'] - 1;
                $this->model->updateById(['goods_num' => $pid_num], $pid_record['id']);
                $this->model->updateById(['goods_num' => $ppid_num], $ppid_record['id']);
            }

            if ($goods['type'] == 2) {
                $param = [];
                $param['goods_parent_serial'] = $goods_serial;
                $param['type'] = 1;
                $param['scan_time'] = 0;
                $goods_list = $this->model->getListByParam($param);
                $goods_list = obj2arr($goods_list);
                $goods_ids = array_column($goods_list, 'id');
                if ($goods_ids) {
                    $editData['goods_num'] = 0;
                    $this->model->updateById($editData, $goods_ids);
                }

                $pid_code = $goods['goods_parent_serial'];
                $param = [];
                $param['goods_serial'] = $pid_code;
                $pid_record = $this->model->getOneByParam($param);
                $pid_num = $pid_record['goods_num'] - $goods['goods_num'];
                $this->model->updateById(['goods_num' => $pid_num], $pid_record['id']);

            }

            if ($goods['type'] == 3) {
                $param = [];
                $param['goods_parent_serial'] = $goods_serial;
                $param['type'] = 2;
                $param['scan_time'] = 0;
                $goods_list = $this->model->getListByParam($param);
                $goods_list = obj2arr($goods_list);
                $goods_ids = array_column($goods_list, 'id');
                $goods_serials = array_column($goods_list, 'goods_serial');
                if ($goods_ids) {
                    $editData['goods_num'] = 0;
                    $this->model->updateById($editData, $goods_ids);
                }

                $param = [];
                $param['goods_parent_serial'] = ['in', $goods_serials];
                $param['type'] = 1;
                $goods_list = $this->model->getListByParam($param);
                $goods_list = obj2arr($goods_list);
                $goods_ids = array_column($goods_list, 'id');
                if ($goods_ids) {
                    $editData['goods_num'] = 0;
                    $this->model->updateById($editData, $goods_ids);
                }
            }
            $editData = [
                'goods_num' => 0,
            ];
            $this->model->updateById($editData, $goods['id']);
            DB::commit();
            return $return_result;
        } catch (Exception $e) {
            DB::rollback();
            apiFail('100000', $e->getTraceAsString());
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/12 16:28
     */
    public function placeOrderWhenScanEnd2C($data = [])
    {
        // 定义对象
        $clerkModel = new ClerkModel();
        $storeModel = new StoreModel();
        $productModel = new ProductModel();
        $commonService = new CommonService();

        // 检查token
        $cache_clerk = $this->getClerkFromRedis($data['token']);
        !$cache_clerk && apiFail('100003', 'token错误或已过期');
        $phone = $cache_clerk['phone'];
        $goods_serial = $data['goods_serial'];

        // 获取当前登录者ID
        $param = [];
        $param['phone'] = $phone;
        $param['store_id'] = $data['store_id'];
        $param['delete_time'] = 0;
        $userInfo = $clerkModel->getOneByParam($param);
        !$userInfo && apiFail('400005', '找不到登录者信息');

        // 获取医院信息
        $param = [];
        $param['id'] = $data['store_id'];
        $param['delete_time'] = 0;
        $store = $storeModel->getOneByParam($param);
        !$store && apiFail('400005', 'storeId error');

        // 获取商品信息
        $param = [];
        $param['goods_serial'] = $data['goods_serial'];
        $goods = $this->model->getOneByParam($param);
        empty($goods) && apiFail('100000', '后台尚未拥有该二维码');
        $goods['useful'] == 0 && apiFail('100002', '无效二维码');


        // 产品图片
        $param = [];
        $param['name'] = $goods['goods_name'];
        $product = $productModel->getOneByParam($param);
        $img_url = '';
        if ($product) {
            $imgs = $commonService->getImgFullUrlFormat($product['img_url']);
            $img_url = $imgs ? $imgs[0]['url'] : '';
        }

        // 已扫描过
        if ($goods['scan_status'] == 1) {
            $return_result = ['coupon_list' => [], 'scan_status' => 2, 'img_url' => $img_url, 'sale_num' => 0, 'give_num' => 0, 'total' => $goods['goods_num'], 'goods_name' => $goods['goods_name']];
            return $return_result;
        }

        // 开始事务
        Db::startTrans();
        try {
            $goods_num = $goods['goods_num'];
            $return_result = $this->getSaleInfo2C($store['id'], $goods['goods_name_code'], $goods_num, $goods['id'], $userInfo['id'], $data['scan_id']);
            $return_result['goods_name'] = $goods['goods_name'];
            $return_result['scan_status'] = 1;
            $return_result['img_url'] = $img_url;
            $this->model->updateGoods($userInfo['id'], $store['id'], $store['agenter_id'], $store['company_id'], $data['address'], $goods['id']);

            if ($goods['type'] == 1) {
                $pid_code = $goods['goods_parent_serial'];
                $param = [];
                $param['goods_serial'] = $pid_code;
                $pid_record = $this->model->getOneByParam($param);
                $ppid_code = $pid_record ? $pid_record['goods_parent_serial'] : '';
                $param = [];
                $param['goods_serial'] = $ppid_code;
                $ppid_record = $this->model->getOneByParam($param);
                $pid_num = $pid_record['goods_num'] - 1;
                $ppid_num = $ppid_record['goods_num'] - 1;
                $this->model->updateById(['goods_num' => $pid_num], $pid_record['id']);
                $this->model->updateById(['goods_num' => $ppid_num], $ppid_record['id']);
            }

            if ($goods['type'] == 2) {
                $param = [];
                $param['goods_parent_serial'] = $goods_serial;
                $param['type'] = 1;
                $param['scan_time'] = 0;
                $goods_list = $this->model->getListByParam($param);
                $goods_list = obj2arr($goods_list);
                $goods_ids = array_column($goods_list, 'id');
                if ($goods_ids) {
                    $this->model->updateById([
                        'scan_status' => 1,
                        'scan_time' => time(),
                        'scan_address' => $data['address'],
                        'clerk_id' => $userInfo['id'],
                        'store_id' => $store['id'],
                        'agenter_id' => $store['agenter_id'],
                        'company_id' => $store['company_id'],
                        'created_at' => time(),
                        'goods_num' => 0
                    ], $goods_ids);
                }
                $pid_code = $goods['goods_parent_serial'];
                $param = [];
                $param['goods_serial'] = $pid_code;
                $pid_record = $this->model->getOneByParam($param);
                $pid_num = $pid_record['goods_num'] - $goods['goods_num'];
                $this->model->updateById(['goods_num' => $pid_num], $pid_record['id']);
            }

            if ($goods['type'] == 3) {
                $param = [];
                $param['goods_parent_serial'] = $goods_serial;
                $param['type'] = 2;
                $param['scan_time'] = 0;
                $goods_list = $this->model->getListByParam($param);
                $goods_list = obj2arr($goods_list);
                $goods_ids = array_column($goods_list, 'id');
                $goods_serials = array_column($goods_list, 'goods_serial');
                if ($goods_ids) {
                    $this->model->updateById([
                        'scan_status' => 1,
                        'scan_time' => time(),
                        'scan_address' => $data['address'],
                        'clerk_id' => $userInfo['id'],
                        'store_id' => $store['id'],
                        'agenter_id' => $store['agenter_id'],
                        'company_id' => $store['company_id'],
                        'created_at' => time(),
                        'goods_num' => 0
                    ], $goods_ids);
                }

                $param = [];
                $param['goods_parent_serial'] = ['in', $goods_serials];
                $param['type'] = 1;
                $goods_list = $this->model->getListByParam($param);
                $goods_list = obj2arr($goods_list);
                $goods_ids = array_column($goods_list, 'id');
                if ($goods_ids) {
                    $this->model->updateById([
                        'scan_status' => 1,
                        'scan_time' => time(),
                        'scan_address' => $data['address'],
                        'clerk_id' => $userInfo['id'],
                        'store_id' => $store['id'],
                        'agenter_id' => $store['agenter_id'],
                        'company_id' => $store['company_id'],
                        'created_at' => time(),
                        'goods_num' => 0
                    ], $goods_ids);
                }
            }
            $editData = [
                'goods_num' => 0,
            ];
            $this->model->updateById($editData, $goods['id']);
            DB::commit();
            return $return_result;
        } catch (Exception $e) {
            DB::rollback();
            apiFail('100000', $e->getTraceAsString());
        }
    }


    /**
     * 规则检测适配
     * 返回是否使用第一规则及适配第一规则的商品数量
     * @param $store_id
     * @param $goods_name
     * @param $goods_num
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/12 14:45
     */
    public function getSaleInfo2B($agenter_id, $goods_name_code, $goods_num, $store_id = 0, $scan_id = '')
    {
        // 定义变量
        $agenterGoodsSaleModel = new AgenterGoodsSaleModel();
        $setModel = new SetModel();
        $storeModel = new StoreModel();
        $return_result = ['coupon_list' => [], 'sale_num' => 0, 'give_num' => 0, 'total' => $goods_num];

        // 获取规则列表
        $couponList = cache('couponrule');
        $limit = isset($couponList[0]['Limit']) ? $couponList[0]['Limit'] : 3;

        // 获取平台设置
        $settingList = $setModel->getListByParam();
        $settingList = obj2arr($settingList);
        $setting_id_value = array_column($settingList, 'value', 'id');

        $couponTpl1 = [
            'product_name_code' => $goods_name_code,
            'x' => $couponList[0]['BuyNumber'],
            'y' => $couponList[0]['GiveNumber'],
            'limit' => $couponList[0]['Limit'],
            'desc' => '',
            'matchNum' => 0,
            'end_time' => date('Y-m-d H:i:s', time() + $setting_id_value[1] * 24 * 60 * 60),
            'start_time' => date('Y-m-d H:i:s', time())
        ];
        $couponTpl2 = [
            'product_name_code' => $goods_name_code,
            'x' => $couponList[1]['BuyNumber'],
            'y' => $couponList[1]['GiveNumber'],
            'limit' => '无上限',
            'desc' => '',
            'matchNum' => 0,
            'end_time' => date('Y-m-d H:i:s', time() + $setting_id_value[2] * 24 * 60 * 60),
            'start_time' => date('Y-m-d H:i:s', time())
        ];

        $param = [];
        $param['goods_name_code'] = $goods_name_code;
        $param['store_id'] = $store_id;
        $order = ['id' => 'asc'];
        $storeGoodsSale = $agenterGoodsSaleModel->getListByParam($param, null, null, null, $order);
        $storeGoodsSale = obj2arr($storeGoodsSale);
        $count = count($storeGoodsSale);
        $record = [];
        if ($storeGoodsSale) {
            $record = $storeGoodsSale[$count - 1];
        }

        $param = [];
        $param['id'] = $store_id;
        $store = $storeModel->getOneByParam($param);
        empty($store) && apiFail('100000', '$store_id error');
        $already_send_121 = $store['send_num_121'];
        $firstDay = $store['send_121_date'];
        $today = date('Y-m-d', time());

        if (empty($store['send_121_date']) || $firstDay == '0000-00-00') {
            if ($already_send_121 < $limit) {
                $un_give_num = $limit - $already_send_121;
                $result1 = $this->rule2B($agenter_id, $goods_name_code, $couponList[0]['BuyNumber'], $goods_num, 0, $store_id, 1, $un_give_num);
                $couponTpl1['matchNum'] = $result1['give'];
                $return_result['sale_num'] += $result1['sale'];
                $return_result['give_num'] += $result1['give'];
                $couponTpl1['matchNum'] && array_push($return_result['coupon_list'], $couponTpl1);
                $sale_num_2 = $result1['left_sale_num'];
            } else {
                $sale_num_2 = $goods_num;
            }
            if ($sale_num_2) {
                $result2 = $this->rule2B($agenter_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, 0, $store_id, 2);
                $couponTpl2['matchNum'] = $result2['give'];
                $return_result['sale_num'] += $result2['sale'];
                $return_result['give_num'] += $result2['give'];
                $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
            }
        } else {
            if (strcmp($firstDay, $today)) {
                $sale_num_2 = $goods_num;
                $left_2 = isset($record['left_num']) ? $record['left_num'] : 0;
                $result3 = $this->rule2B($agenter_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, $left_2, $store_id, 2);
                $couponTpl2['matchNum'] = $result3['give'];
                $return_result['sale_num'] += $result3['sale'];
                $return_result['give_num'] += $result3['give'];
                $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
            } else {
                // 查询首次购买当天的销量
                $param = [];
                $param['store_id'] = $store_id;
                $param['goods_name_code'] = $goods_name_code;
                $param['created_at'] = ['between', [$today . " 00:00:00", $today . " 23:59:59"]];
                $order = ['id' => 'desc'];
                $recode_list = $agenterGoodsSaleModel->getListByParam($param, null, null, null, $order);
                $recode_list = obj2arr($recode_list);

                // 如果首次购买当天销量突破了上线，则不适用第一规则
                if ($already_send_121 >= $limit) {
                    $sale_num_2 = $goods_num;
                    $left_2 = empty($recode_list[0]['left_num']) ? 0 : $recode_list[0]['left_num'];
                    $result4 = $this->rule2B($agenter_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, $left_2, $store_id, 2);
                    $couponTpl2['matchNum'] = $result4['give'];
                    $return_result['sale_num'] += $result4['sale'];
                    $return_result['give_num'] += $result4['give'];
                    $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
                    return $return_result;
                }

                // 如果首次购买当天还没有突破了上线，则适用第一规则
                $un_give_num = $limit - $already_send_121;
                $left = empty($recode_list[0]['left_num']) ? 0 : $recode_list[0]['left_num'];
                // 第一规则
                $result5 = $this->rule2B($agenter_id, $goods_name_code, $couponList[0]['BuyNumber'], $goods_num, $left, $store_id, 1, $un_give_num);
                $couponTpl1['matchNum'] = $result5['give'];
                $return_result['sale_num'] += $result5['sale'];
                $return_result['give_num'] += $result5['give'];
                $couponTpl1['matchNum'] && array_push($return_result['coupon_list'], $couponTpl1);
                if ($result5['left_sale_num']) {
                    $sale_num_2 = $result5['left_sale_num'];
                    $result6 = $this->rule2B($agenter_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, 0, $store_id, 2);
                    $couponTpl2['matchNum'] = $result6['give'];
                    $return_result['sale_num'] += $result6['sale'];
                    $return_result['give_num'] += $result6['give'];
                    $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
                }
            }
        }

        return $return_result;
    }

    /**
     * 规则检测适配
     * 返回是否使用第一规则及适配第一规则的商品数量
     * @param $store_id
     * @param $goods_name
     * @param $goods_num
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/12 14:45
     */
    public function getSaleInfo2C($store_id, $goods_name_code, $goods_num, $goods_id = 0, $clerk_id = 0, $scan_id = '')
    {
        // 定义对象
        $storeGoodsSaleModel = new StoreGoodsSaleModel();
        $couponsModel = new CouponsModel();
        $setModel = new SetModel();
        $storeModel = new StoreModel();

        // 初始化返回
        $return_result = ['coupon_list' => [], 'sale_num' => 0, 'give_num' => 0, 'total' => $goods_num];

        // 获取平台设置
        $settingList = $setModel->getListByParam();
        $settingList = obj2arr($settingList);
        $setting_id_value = array_column($settingList, 'value', 'id');

        $next_day_time = time() + 24 * 60 * 60;
        $next_day = date("Y-m-d", $next_day_time);
        $next_day_start = $next_day . " 00:00:00";
        $end1 = strtotime($next_day_start) + $setting_id_value[1] * 24 * 60 * 60;
        $end2 = strtotime($next_day_start) + $setting_id_value[2] * 24 * 60 * 60 - 1;
        // 获取规则列表
        $couponList = cache('couponrule');
        $limit = isset($couponList[0]['Limit']) ? $couponList[0]['Limit'] : 3;
        $couponTpl1 = [
            'x' => $couponList[0]['BuyNumber'],
            'y' => $couponList[0]['GiveNumber'],
            'limit' => $couponList[0]['Limit'],
            'desc' => '本次优惠以医院为单位',
            'matchNum' => 0,
            'end_time' => date('Y-m-d H:i:s', $end1),
            'start_time' => date('Y-m-d H:i:s', strtotime($next_day_start)),
            'type' => 1 // 首次购买
        ];
        $couponTpl2 = [
            'product_name_code' => $goods_name_code,
            'x' => $couponList[1]['BuyNumber'],
            'y' => $couponList[1]['GiveNumber'],
            'limit' => '无上限',
            'desc' => '本次优惠以医院为单位',
            'matchNum' => 0,
            'end_time' => date('Y-m-d H:i:s', $end2),
            'start_time' => date('Y-m-d H:i:s', strtotime($next_day_start)),
            'type' => 2 // 二次购买或购买量超过首次购买上线
        ];

        // 获取当前时间的前一天时间
        $preDate = date('Y-m-d', strtotime('-1 day'));
        $preDateTime = strtotime($preDate . " 23:59:59");

        // 获取该商品的优惠券列表
        $param = [];
        $param['goods_name_code'] = $goods_name_code;
        $param['store_id'] = $store_id;
        $param['status'] = 0;
        $param['end_time'] = array('>', time());
        $param['created_at'] = array('<', $preDateTime);
        $param['type'] = 1;
        $coupons_1 = $couponsModel->getListByParam($param);
        $param['type'] = 2;
        $coupons_2 = $couponsModel->getListByParam($param);
        $coupons_1 = obj2arr($coupons_1);
        $coupons_2 = obj2arr($coupons_2);

        // 首次购买
        $coupons_1_cnt = count($coupons_1);
        $final_cnt_1 = $goods_num >= $coupons_1_cnt ? $coupons_1_cnt : $goods_num;
        $id1s = [];
        $id2s = [];
        for ($i = 0; $i < $final_cnt_1; $i++) {
            array_push($id1s, $coupons_1[$i]['id']);
        }

        // 二次购买或者超过首次购买上线获取到的优惠券类型
        $max_song_num = ($goods_num - $final_cnt_1);
        $coupons_2_cnt = count($coupons_2);
        $final_cnt_2 = $max_song_num >= $coupons_2_cnt ? $coupons_2_cnt : $max_song_num;
        for ($i = 0; $i < $final_cnt_2; $i++) {
            array_push($id2s, $coupons_2[$i]['id']);
        }

        // 统计优惠数量
        if ($id1s) {
            $editData = ['status' => 1];
            $couponsModel->updateById($editData, $id1s);
        }
        if ($id2s) {
            $editData = ['status' => 1];
            $couponsModel->updateById($editData, $id2s);
        }
        $song_num_1 = count($id1s);
        $song_num_2 = count($id2s);
        $left_good_num = $goods_num - $song_num_1 - $song_num_2;

        // 最后一条购买记录
        $param = [];
        $param['goods_name_code'] = $goods_name_code;
        $param['store_id'] = $store_id;
        $order = ['id' => 'asc'];
        $storeGoodsSale = $storeGoodsSaleModel->getListByParam($param, null, null, null, $order); // 获取门店采购该商品的列表
        $storeGoodsSale = obj2arr($storeGoodsSale);
        $count = count($storeGoodsSale);
        $record = [];
        if ($storeGoodsSale) {
            $record = $storeGoodsSale[$count - 1];
        }


        // 获取门店信息
        $param = [];
        $param['id'] = $store_id;
        $store = $storeModel->getOneByParam($param);
        empty($store) && apiFail('100000', '$store_id error');
        $already_send_121 = $store['send_num_121'];
        $firstDay = $store['send_121_date'];
        $today = date('Y-m-d', time());

        // 购买逻辑处理
        if (empty($firstDay) || $firstDay == '0000-00-00') {   // 首次购买
            if ($already_send_121 < $limit) { // 尚未达到首次购买的上限
                $un_give_num = $limit - $already_send_121;
                $sale_num_1 = $left_good_num >= $un_give_num * $couponList[0]['BuyNumber'] ? $un_give_num * $couponList[0]['BuyNumber'] : $left_good_num;
                $result1 = $this->rule2C($clerk_id, $store_id, $goods_name_code, $couponList[0]['BuyNumber'], $sale_num_1, 0, $song_num_1);
                $couponTpl1['matchNum'] = $result1['give'];
                $return_result['sale_num'] += $result1['sale'];
                $return_result['give_num'] += $result1['give'];
                $couponTpl1['matchNum'] && array_push($return_result['coupon_list'], $couponTpl1);
                $sale_num_2 = $left_good_num >= $un_give_num * $couponList[0]['BuyNumber'] ? $left_good_num - $un_give_num * $couponList[0]['BuyNumber'] : 0;
            } else {
                $sale_num_2 = $left_good_num;
            }
            $result2 = $this->rule2C($clerk_id, $store_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, 0, $song_num_2, 2);
            $couponTpl2['matchNum'] = $result2['give'];
            $return_result['sale_num'] += $result2['sale'];
            $return_result['give_num'] += $result2['give'];
            $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
        } else {   // 非首次采购
            if (strcmp($firstDay, $today)) { // 但扫描时间已经不是首次购买的日期
                $sale_num_2 = $left_good_num;
                $left = isset($record['left_num']) ? $record['left_num'] : 0;
                $result2 = $this->rule2C($clerk_id, $store_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, $left, $song_num_1 + $song_num_2, 2);
                $couponTpl2['matchNum'] = $result2['give'];
                $return_result['sale_num'] += $result2['sale'];
                $return_result['give_num'] += $result2['give'];
                $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
            } else {
                // 查询首次购买当天的销量
                $param = [];
                $param['store_id'] = $store_id;
                $param['goods_name_code'] = $goods_name_code;
                $param['created_at'] = ['between', [$today . " 00:00:00", $today . " 23:59:59"]];
                $order = ['id' => 'desc'];
                $recode_list = $storeGoodsSaleModel->getListByParam($param, null, null, null, $order);
                $recode_list = obj2arr($recode_list);
                // 如果首次购买当天销量突破了上线，则不适用第一规则
                if ($store['send_num_121'] >= $limit) {
                    $sale_num_2 = $left_good_num;
                    $left = empty($recode_list[0]['left_num']) ? 0 : $recode_list[0]['left_num'];
                    $result2 = $this->rule2C($clerk_id, $store_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, $left, $song_num_1 + $song_num_2, 2);
                    $couponTpl2['matchNum'] = $result2['give'];
                    $return_result['sale_num'] += $result2['sale'];
                    $return_result['give_num'] += $result2['give'];
                    $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
                } else {
                    // 如果首次购买当天还没有突破了上线，则适用第一规则
                    $un_give_num = $limit - $store['send_num_121'];
                    $left = empty($recode_list[0]['left_num']) ? 0 : $recode_list[0]['left_num'];
                    $sale_num = $left_good_num >= ($un_give_num * $couponList[0]['BuyNumber']) ? $un_give_num * $couponList[0]['BuyNumber'] : $left_good_num;
                    $result1 = $this->rule2C($clerk_id, $store_id, $goods_name_code, $couponList[0]['BuyNumber'], $sale_num, $left, $song_num_1);
                    $couponTpl1['matchNum'] = $result1['give'];
                    $return_result['sale_num'] += $result1['sale'];
                    $return_result['give_num'] += $result1['give'];
                    $couponTpl1['matchNum'] && array_push($return_result['coupon_list'], $couponTpl1);

                    $sale_num_2 = $left_good_num >= $un_give_num * $couponList[0]['BuyNumber'] ? $left_good_num - $un_give_num * $couponList[0]['BuyNumber'] : 0;
                    $result2 = $this->rule2C($clerk_id, $store_id, $goods_name_code, $couponList[1]['BuyNumber'], $sale_num_2, 0, $song_num_2, 2);
                    $couponTpl2['matchNum'] = $result2['give'];
                    $return_result['sale_num'] += $result2['sale'];
                    $return_result['give_num'] += $result2['give'];
                    $couponTpl2['matchNum'] && array_push($return_result['coupon_list'], $couponTpl2);
                }
            }
        }

        $this->addCoupon($return_result, $setting_id_value, $goods_id, $goods_name_code, $store_id, $scan_id);
        return $return_result;
    }

    /**
     * @Author Zed
     * @Time 2019/4/30 14:58
     */
    public function addCoupon($return_result, $setting_id_value, $goods_id, $goods_name_code, $store_id, $scan_id)
    {
        $couponsModel = new CouponsModel();

        $next_day_time = time() + 24 * 60 * 60;
        $next_day = date("Y-m-d", $next_day_time);
        $next_day_start = $next_day . " 00:00:00";

        // add coupon
        $addData = [];
        foreach ($return_result['coupon_list'] as $k => $v) {
            $type = $v['type'];
            if ($type == 1) {
                $end_time = strtotime($next_day_start) + $setting_id_value[1] * 24 * 60 * 60 - 1;
            } else {
                $end_time = strtotime($next_day_start) + $setting_id_value[2] * 24 * 60 * 60 - 1;
            }
            for ($i = 0; $i < $v['matchNum']; $i++) {
                array_push($addData, [
                    'goods_id' => $goods_id,
                    'goods_name_code' => $goods_name_code,
                    'type' => $type,
                    'status' => 0,
                    'store_id' => $store_id,
                    'created_at' => time(),
                    'start_time' => strtotime($next_day_start),
                    'end_time' => $end_time,
                    'scan_id' => $scan_id,
                    'buy' => $v['x'],
                    'give' => $v['y']
                ]);
            }
        }
        $couponsModel->addList($addData);
    }

    /**
     * @param $agenter_id
     * @param $goods_name_code
     * @param int $BuyNumber
     * @param int $total
     * @param int $left
     * @param int $sale
     * @param int $give
     * @param $store_id
     * @return array
     * @throws exception
     * @Author Zed
     * @Time 2019/4/17 11:07
     */
    public function rule2B($agenter_id, $goods_name_code, $BuyNumber = 1, $sale_num = 0, $left = 0, $store_id, $type = 1, $un_give_first = 0)
    {
        $agenterGoodsSaleModel = new AgenterGoodsSaleModel();
        $storeModel = new StoreModel();

        $left_give = 0;
        $real_left = $left;
        if ($sale_num > 0 && $left > 0 && ($left >= $BuyNumber)) {
            $left_give = intval($left / $BuyNumber);
            if ($type == 1) {
                $left_give = $left_give > $un_give_first ? $un_give_first : $left_give;
            }
            if ($sale_num > $left_give) {
                $sale_num = $sale_num - $left_give;
                $real_left = 0;
            } else {
                $left_give = $sale_num;
                $sale_num = 0;
                $real_left = $left - $left_give * $BuyNumber;
            }
        }
        $left = $real_left;
        $left_sale_num = 0;
        // 第一规则
        if ($type == 1) {
            $give = 0;
            $real_sale = 0;
            for ($i = 1; $i <= $sale_num; $i++) {
                if ($un_give_first <= 0) {
                    break;
                }

                if ($left >= $BuyNumber) {
                    $left = 0;
                    $give++;
                    $un_give_first--;
                    continue;
                }
                $real_sale++;
                $left++;
            }

            $left_sale_num = $sale_num - $real_sale - $give;
        }


        if ($type == 2) {
            $give = 0;
            $real_sale = 0;
            for ($i = 1; $i <= $sale_num; $i++) {
                if ($left >= $BuyNumber) {
                    $left = 0;
                    $give++;
                    continue;
                }
                $real_sale++;
                $left++;
            }
        }

        $str = $type == 1 ? '首次' : '二次';
        $desc = $str . '购买,' . "当前使用规则为买" . $BuyNumber . "送1。" . "left_give=" . $left_give . ",real_left=" . $real_left;

        $addData = [
            'store_id' => $store_id,
            'agenter_id' => $agenter_id,
            'goods_name_code' => $goods_name_code,
            'goods_name' => $goods_name_code,
            'total_num' => $real_sale + $give + $left_give,
            'sale_num' => $real_sale,
            'give_num' => $give + $left_give,
            'left_num' => $left,
            'created_at' => date('Y-m-d H:i:s'),
            'desc' => $desc
        ];
        if (($sale_num + $left_give + $give) > 0) {
            $agenterGoodsSaleModel->add($addData);
            $param = [];
            $param['id'] = $store_id;
            $store = $storeModel->getOneByParam($param);
            if (empty($store)) {
                apiFail('100000', '$store_id error');
            }

            $editData = [
                'buy_num' => $store['buy_num'] + $addData['sale_num'],
                'give_num' => $store['give_num'] + $addData['give_num']
            ];
            if ($type == 1) {
                $editData['send_num_121'] = $store['send_num_121'] + $give + $left_give;
            }
            if (empty($store['send_121_date']) || $store['send_121_date'] == '0000-00-00') {
                $editData['send_121_date'] = date('Y-m-d', time());
            }
            $storeModel->updateById($editData, $store['id']);
        }

        return [
            'total' => $sale_num,
            'left' => $left,
            'sale' => $real_sale,
            'give' => $give + $left_give,
            'left_sale_num' => $left_sale_num
        ];
    }

    /**
     * @param $store_id
     * @param $goods_name_code
     * @param int $BuyNumber
     * @param int $total
     * @param int $left
     * @param int $sale
     * @param int $give
     * @param int $youhui_cnt
     * @return array
     * @throws exception
     * @Author Zed
     * @Time 2019/3/19 14:36
     */
    public function rule2C($clerk_id, $store_id, $goods_name_code, $BuyNumber = 1, $sale_num = 0, $left = 0, $song = 0, $type = 1)
    {
        $storeGoodsSaleModel = new StoreGoodsSaleModel();
        $storeModel = new StoreModel();

        $left_give = 0;
        $real_left = 0;
        if ($sale_num > 0 && $left > 0 && ($left >= $BuyNumber)) {
            $real_left = $left % $BuyNumber;
            $left_give = intval($left / $BuyNumber);
            $left = $real_left;
        }


        $give = 0;
        for ($i = 1; $i <= $sale_num; $i++) {
            $left++;
            if ($left >= $BuyNumber) {
                $give++;
                $left = 0;
            }
        }
        $str = $type == 1 ? '首次' : '二次';
        $desc = $str . '购买,' . "当前使用规则为买" . $BuyNumber . "送1。" . "left_give=" . $left_give . ",real_left=" . $real_left . ",song=" . $song;

        $addData = [
            'clerk_id' => $clerk_id,
            'store_id' => $store_id,
            'goods_name_code' => $goods_name_code,
            'goods_name' => $goods_name_code,
            'total_num' => $sale_num + $song,
            'sale_num' => $sale_num,
            'give_num' => $give + $left_give,
            'left_num' => $left,
            'created_at' => date('Y-m-d H:i:s'),
            'song_num' => $song,
            'desc' => $desc
        ];
        // 新增一条门店销售记录
        if (($sale_num + $song) > 0) {
            $storeGoodsSaleModel->add($addData);
            // 销量更新到门店销量和赠送量中
            $param = [];
            $param['id'] = $store_id;
            $store = $storeModel->getOneByParam($param);
            empty($store) && apiFail('100000', '$store_id error');
            $editData = ['buy_num' => $store['buy_num'] + $addData['sale_num'], 'give_num' => $store['give_num'] + $addData['song_num']];
            if ($type == 1) {
                $editData['send_num_121'] = $store['send_num_121'] + $give;
            }
            if (empty($store['send_121_date']) || $store['send_121_date'] == '0000-00-00') {
                $editData['send_121_date'] = date('Y-m-d', time());
            }
            $storeModel->updateById($editData, $store['id']);
        }

        // 返回
        return ['total' => $sale_num + $song, 'left' => $left, 'sale' => $sale_num, 'give' => $give + $left_give];
    }
}
