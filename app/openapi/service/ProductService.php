<?php
namespace app\openapi\service;

use app\comm\model\AgenterModel;
use app\comm\model\ProductModel;
use app\comm\model\StoreModel;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class ProductService extends BaseService
{
    private $productModel= null;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->productModel = new ProductModel();
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 15:16
     */
    public function getProductList($data = array())
    {
        $commonSrvice = new CommonService();
        $storeModel = new StoreModel();
        $agenterModel = new AgenterModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }

        $param = [];
        $param['delete_time'] = 0;
        $productList = $this->productModel->getListByParam($param);
        $productList = obj2arr($productList);
        foreach ($productList as $k => $v) {
            $productList[$k]['img_url'] = $commonSrvice->getImgFullUrl(explode(',' ,$v['img_url']));
        }
        return $productList;
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 15:16
     */
    public function getProductInfo($data = array())
    {
        $commonSrvice = new CommonService();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $param = [];
        $param['delete_time'] = 0;
        $param['id'] = $data['product_id'];
        $product = $this->productModel->getOneByParam($param);
        if (!$product) {
            apiFail('300002', '没有该产品');
        }
        $product['img_url'] = $commonSrvice->getImgFullUrl(explode(',' ,$product['img_url']));
        return $product;
    }
}
