<?php

namespace app\openapi\service;

use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\FeatureModel;
use app\comm\model\RegionModel;
use app\comm\model\StoreGoodsSaleModel;
use app\comm\model\StoreModel;
use think\Cache;
use think\Db;
use think\Exception;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class StoreService extends BaseService
{
    private $storeModel = null;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->storeModel = new StoreModel();
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 9:31
     */
    public function register($data = array())
    {
        $clerkModel = new ClerkModel();

        $city_id = 0;
        $area_id = 0;
        $data['province'] = empty($data['province']) ? 0 : $data['province'];
        $data['city'] = empty($data['city']) ? 0 : $data['city'];
        $data['area'] = empty($data['area']) ? 0 : $data['area'];
        $province_id = $this->getRegionIdByName($data['province']);
        $province_id && $city_id = $this->getRegionIdByName($data['city'], $province_id);
        $city_id && $area_id = $this->getRegionIdByName($data['area'], $city_id);
        Db::startTrans();
        try {
            $addData = [
                'store_imgs' => $data['store_img_ids'],
                'store_name' => $data['store_name'],
                'province_id' => $province_id,
                'city_id' => $city_id,
                'area_id' => $area_id,
                'address_detail' => $data['address_detail'],
                'company_id' => 0,
                'agenter_id' => 0,
                'store_owner_id' => 0,
                'contact_name' => $data['contact_name'],
                'contact_phone' => $data['contact_phone'],
                'check_status' => 0,
                'check_reason' => '',
                'delete_time' => 0,
                'lon' => $data['lon'],
                'lat' => $data['lat'],
            ];
            $id = $this->storeModel->add($addData);

            $addData2 = [
                'name' => $data['contact_name'],
                'phone' => $data['contact_phone'],
                'position' => 2,
                'is_use' => 1,
                'store_id' => $id
            ];
            $user_id = $clerkModel->add($addData2);
            $this->storeModel->updateById(['store_owner_id' => $user_id], $id);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            apiFail('100000', $e->getMessage());
        }

    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function getStoreInfo($data = array())
    {
        $clerkModel = new ClerkModel();
        $commonService = new CommonService();

        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone = $cache_clerk['phone'];

        $param = [];
        $param['id'] = $data['store_id'];
        $param['check_status'] = 1; // 审核通过
        $param['delete_time'] = 0;
        $store = $this->storeModel->getOneByParam($param);
        if (!$store) {
            apiFail('200004', '医院ID错误');
        }
        $store['store_imgs'] = empty($store['store_imgs']) ? [] : $commonService->getImgFullUrlFormat(explode(',', $store['store_imgs']));
        $store['province_name'] = $this->getRegionNameById($store['province_id']);
        $store['city_name'] = $this->getRegionNameById($store['city_id']);
        $store['area_name'] = $this->getRegionNameById($store['area_id']);

        // 获取店长个人信息
        $param = [];
        $param['store_id'] = $data['store_id'];
        $param['position'] = 2; // 店长
        $param['delete_time'] = 0;
        $owner_clerk = $clerkModel->getOneByParam($param);
        if (!$owner_clerk) {
            apiFail('200005', '该门店还没有非配店长');
        }

        // 获取当前登录者信息
        $param = [];
        $param['phone'] = $phone;
        $param['store_id'] = $data['store_id'];
        $current_clerk = $clerkModel->getOneByParam($param);
        if (!$current_clerk) {
            apiFail('200006', '数据异常');
        }
        $current_clerk['position_name'] = getPositionById($current_clerk['position']);

        // 获取当前门店的的助理和医生列表
        $clerkList = [];
        if ($current_clerk['position'] == 2) {
            $param = [];
            $param['store_id'] = $data['store_id'];
            $param['position'] = ['in', [0, 1]];
            $param['delete_time'] = 0;
            $clerkList = $clerkModel->getListByParam($param);
            $clerkList = obj2arr($clerkList);
            foreach ($clerkList as $k => $v) {
                $clerkList[$k]['position_name'] = getPositionById($v['position']);
            }
        }

        $result = [
            'store_info' => $store,
            'shop_owner_info' => $owner_clerk,
            'clerk_info' => $current_clerk,
            'store_clerk_list' => $clerkList
        ];
        return $result;
    }

    /**
     * @param $id
     * @return mixed|string
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/14 15:16
     */
    public function getRegionNameById($id)
    {
        $regionModel = new RegionModel();

        $cache = Cache::store('default')->get('region_' . $id);
        if (empty($cache)) {
            $param = [];
            $param['id'] = $id;
            $cache = $regionModel->getOneByParam($param);
            Cache::store('default')->set('region_' . $id, $cache, 30 * 60 * 24 * 24);
        }

        return isset($cache['name']) ? $cache['name'] : '';
    }

    /**
     * 获取特色列表
     * @Author Zed
     * @Time 2019/3/6 11:03
     */
    public function getTsList()
    {
        $featureModel = new FeatureModel();
        $param = [];
        $list = $featureModel->getListByParam($param);
        return ['list' => $list];
    }

    /**
     * 代理人新增医院
     * @Author Zed
     * @Time 2019/3/6 11:03
     */
    public function createStore($data = [])
    {
        $agenterModel = new AgenterModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone = $clerk['phone'];

        // 查询代理是否存在
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if (!$agenter) {
            apiFail('10001', '该手机号对应的代理已删除');
        }
        // 获取代理商信息
        $param = [];
        $param['phone'] = $data['clerk_phone'];
        $param['delete_time'] = 0;
        $is_agenter = $agenterModel->getOneByParam($param);
        if ($is_agenter) {
            apiFail('100002', '该号码已注册成为代理商，不能再成为店长');
        }
        $city_id = 0;
        $area_id = 0;
        $province_id = $this->getRegionIdByName($data['province']);
        $province_id && $city_id = $this->getRegionIdByName($data['city'], $province_id);
        $city_id && $area_id = $this->getRegionIdByName($data['area'], $city_id);

        $addData = [
            'store_imgs' => $data['store_imgs'],
            'store_name' => $data['name'],
            'province_id' => $province_id,
            'city_id' => $city_id,
            'area_id' => $area_id,
            'address_detail' => $data['address_detail'],
            'company_id' => $agenter['company_id'],
            'agenter_id' => $agenter['id'],
            'contact_name' => $data['clerk_name'],
            'contact_phone' => $data['clerk_phone'],
            'check_status' => 1,
            'check_reason' => '',
            'delete_time' => 0,
            'scope' => isset($data['scope']) ? $data['scope'] : 0,
            'desc' => isset($data['desc']) ? $data['desc'] : '',
            'ts' => $data['ts'],
            'lon' => $data['lon'],
            'lat' => $data['lat'],
            'create_time' => time(),
        ];
        $store_id = $this->storeModel->add($addData);
        if ($store_id) {
            $this->addStoreMananger($data['clerk_phone'], $data['clerk_name'], $store_id);
        }
        return ['store_id' => $store_id];
    }

    /**
     * 添加或更新店长信息
     * @Author Zed
     * @Time 2019/3/25 12:06
     */
    public function addStoreMananger($phone = '', $name = '', $store_id = 0)
    {
        $agenterModel = new AgenterModel();
        $clerkModel = new ClerkModel();
        $storeModel = new StoreModel();

        if (empty($phone) || empty($name) || empty($store_id)) {
            apiFail('100002', '手机号码|名称不能为空');
        }

        // 获取代理商信息
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if ($agenter) {
            apiFail('100002', '该号码已注册成为代理商，不能再成为店长');
        }

        // 获取用户信息
        $param = [];
        $param['phone'] = $phone;
        $param['store_id'] = $store_id;
        $param['delete_time'] = 0;
        $clerk = $clerkModel->getOneByParam($param);
        if (!$clerk) {
            $addDtata = [
                'store_id' => $store_id,
                'name' => $name,
                'phone' => $phone,
                'position' => 2,
                'delete_time' => 0,
                'created_at' => time(),
                'openid' => '',
            ];
            $new_clerk_id = $clerkModel->add($addDtata);
            $storeModel->updateById(['store_owner_id' => $new_clerk_id], $store_id);
        } else {
            $editData = ['name' => $name];
            $clerkModel->updateById($editData, $clerk['id']);
            $storeModel->updateById(['store_owner_id' => $clerk['id']], $store_id);
        }
    }

    /**
     * 代理人编辑医院
     * @Author Zed
     * @Time 2019/3/6 11:03
     */
    public function editStore($data = [])
    {
        $agenterModel = new AgenterModel();
        $clerkModel = new ClerkModel();
        $storeModel = new StoreModel();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone = $clerk['phone'];

        // 查询代理是否存在
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if (!$agenter) {
            apiFail('10001', '该手机号对应的代理已删除');
        }

        $city_id = 0;
        $area_id = 0;
        $province_id = $this->getRegionIdByName($data['province']);
        $province_id && $city_id = $this->getRegionIdByName($data['city'], $province_id);
        $city_id && $area_id = $this->getRegionIdByName($data['area'], $city_id);

        $editData = [
            'store_name' => $data['name'],
            'province_id' => $province_id,
            'city_id' => $city_id,
            'area_id' => $area_id,
            'address_detail' => $data['address_detail'],
            'scope' => isset($data['scope']) ? $data['scope'] : 0,
            'desc' => isset($data['desc']) ? $data['desc'] : '',
            'ts' => $data['ts'],
            'lon' => $data['lon'],
            'lat' => $data['lat']
        ];
        if (!empty($data['store_imgs'])) {
            $editData['store_imgs'] = $data['store_imgs'];
        }
        $this->storeModel->updateById($editData, $data['store_id']);

        // 手机号不能是已存在在代理人表中
        $agenterModel = new AgenterModel();
        $param = [];
        $param['phone'] = $data['clerk_phone'];
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if ($agenter) {
            apiFail('100006', '该手机号是代理人，不能加入店员身份');
        }

        $param = [];
        $param['id'] = $data['clerk_id'];
        $param['delete_time'] = 0;
        $clerk = $clerkModel->getOneByParam($param);
        if (!$clerk) {
            apiFail('100007', 'clerk_id错误');
        }

        // 查重
        $param = [];
        $param['store_id'] = $data['store_id'];
        $param['phone'] = $data['clerk_phone'];
        $param['id'] = ['not in', [$data['clerk_id']]];
        $param['delete_time'] = 0;
        $clerk = $clerkModel->getOneByParam($param);
        if ($clerk) {
            apiFail('100006', '该手机号已存在当前医院中，不能重复');
        }

        $editDtata = [
            'name' => $data['clerk_name'],
            'phone' => $data['clerk_phone'],
        ];
        $addData = [
            'name' => $data['clerk_name'],
            'phone' => $data['clerk_phone'],
            'position' => 2,
            'is_use' => 1,
            'store_id' => $data['store_id']
        ];
        $clerk_id = $clerkModel->add($addData);
        if (!$clerk_id) {
            apiFail('100006', '编辑失败');
        }
        $StoreGoodsSaleModel = new StoreGoodsSaleModel();

        $StoreGoodsSaleModel->save(['clerk_id' => $clerk_id], ['clerk_id' => $data['clerk_id'], 'store_id' => $data['store_id']]);

        $storeModel->updateById(['store_owner_id' => $clerk_id], $data['store_id']);
        $clerkModel->updateById(['delete_time' => time()], $data['clerk_id']);
    }

    /**
     * 获取医院排名
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/11 10:12
     */
    public function getStoreRank($data = [])
    {
        $storeModel = new StoreModel();
        $commonService = new CommonService();

        $param = [];
        $param['delete_time'] = 0;
        $param['id'] = $data['store_id'];
        $store = $storeModel->getOneByParam($param);
        if (!$store) {
            apiFail('100000', '医院id错误');
        }
        $store_buy_num = $store['buy_num'];

        // 全国排名
        $param = [];
        $param['buy_num'] = ['>=', $store_buy_num];
        $param['check_status'] = 1;
        $param['delete_time'] = 0;
        $cn_rank = $storeModel->getCntByParam($param);


        // 省排名
        $province_id = $store['province_id'];
        $province_list = $commonService->getProvinceList();
        $province_list = $province_list['list'];
        $province_list = obj2arr($province_list);
        $province_id_name_list = array_column($province_list, 'name', 'id');
        $province_name = isset($province_id_name_list[$province_id]) ? $province_id_name_list[$province_id] : '所在省';

        $param = [];
        $param['buy_num'] = ['>=', $store_buy_num];
        $param['check_status'] = 1;
        $param['delete_time'] = 0;
        $param['province_id'] = $province_id;
        $pr_rank = $storeModel->getCntByParam($param);

        $city_id = $store['city_id'];
        $city_list = $commonService->getCityList(['province_id' => $province_id]);
        $city_list = $city_list['list'];
        $city_list = obj2arr($city_list);
        $city_id_name_list = array_column($city_list, 'name', 'id');
        $city_name = isset($city_id_name_list[$city_id]) ? $city_id_name_list[$city_id] : '所在市';

        $param = [];
        $param['buy_num'] = ['>=', $store_buy_num];
        $param['check_status'] = 1;
        $param['delete_time'] = 0;
        $param['city_id'] = $city_id;
        $ci_rank = $storeModel->getCntByParam($param);

        $param = [];
        $param['check_status'] = 1;
        $param['delete_time'] = 0;
        $param['city_id'] = $city_id;
        $ci_total = $storeModel->getCntByParam($param);
        $percent = (floor($ci_total - $ci_rank + 1) / (1.00 * $ci_total)) * 100.00;


        return [
            'cn_rank' => $cn_rank,
            'pr_rank' => $pr_rank,
            'ci_rank' => $ci_rank,
            'ci_percent' => number_format($percent, 2),
            'province_name' => $province_name,
            'city_name' => $city_name
        ];
    }

    public function verifyAddress($data = '')
    {
        $store_model = new StoreModel();
        $store_model->where(function ($query) use ($data) {
            $query->where(['delete_time' => 0]);
            $query->where(['lat' => $data['lat'], 'lon' => $data['lon']]);
        });
        $store_model->whereOr(function ($query) use ($data) {
            $query->where(['delete_time' => 0]);
            $query->where(['address_detail' => trim($data['address_detail'])]);
        });
        $info = $store_model->find();
        return $info;
    }
}
