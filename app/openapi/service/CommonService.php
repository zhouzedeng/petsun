<?php
namespace app\openapi\service;

use app\comm\model\AdModel;
use app\comm\model\ImgsModel;
use app\comm\model\RegionModel;
use think\Cache;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class CommonService
{

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:14
     */
    public function getAdList()
    {
        $adModel = new AdModel();
        $param = [];
        $adList = $adModel->getListByParam($param);
        $adList = obj2arr($adList);
        foreach ($adList as $k => $v) {
            $adList[$k]['img_url'] = $this->getImgFullUrl([$v['img_url']]);
        }
        return ['adList' => $adList];
    }

    /**
     * @param array $img_ids
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/11 16:25
     */
    public function getImgFullUrl($img_ids = [])
    {
        $imgModel = new ImgsModel();

        if (!is_array($img_ids)) {
            $img_ids = [$img_ids];
        }

        $cache_img_list = [];
        $no_cache_ids = [];
        foreach ($img_ids as $k => $img_id) {
            $img = Cache::store('default')->get('imgs_' . $img_id);
            if ($img) {
                $cache_img_list[] = $img;
                continue;
            }
            $no_cache_ids[] = $img_id;
        }

        $param = [];
        $param['id'] = ['in', $no_cache_ids];
        $img_list = $imgModel->getListByParam($param);
        $img_list = obj2arr($img_list);
        $tmpList = [];
        foreach ($cache_img_list as $v) {
            array_push($tmpList, $v);
        }
        foreach ($img_list as $v) {
            array_push($tmpList, $v);
        }

        foreach ($tmpList as $k => $v) {
            $tmpList[$k]['img_name'] = cmf_get_image_url($v['img_name']);
            Cache::store('default')->set('imgs_' . $v['id'], $v, 30 * 24 * 60 *60);
        }
        $tmpList = array_column($tmpList, 'img_name');
        return $tmpList;
    }

    /**
     * @param array $img_ids
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/11 16:25
     */
    public function getImgFullUrlFormat($img_ids = [])
    {
        $imgModel = new ImgsModel();

        if (!is_array($img_ids)) {
            $img_ids = [$img_ids];
        }
        $param = [];
        $param['id'] = ['in', $img_ids];
        $img_list = $imgModel->getListByParam($param);
        $img_list = obj2arr($img_list);
        foreach ($img_list as $k => $v) {
            $img_list[$k]['img_name'] = cmf_get_image_url($v['img_name']);
        }
        $img_list = array_column($img_list, 'img_name', 'id');
        $tmp = [];
        foreach ($img_list as $k => $v) {
            $tmp[] = [
                'id' => $k,
                'url' => $v
            ];
        }
        return $tmp;
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:14
     */
    public function getProvinceList()
    {
        $regionModel = new RegionModel();
        $param = [];
        $param['type'] = 0;
        $list = $regionModel->getListByParam($param);
        return ['list' => $list];
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:14
     */
    public function getCityList($data = array())
    {
        $regionModel = new RegionModel();
        $param = [];
        $param['type'] = 1;
        $param['pid'] = $data['province_id'];
        $list = $regionModel->getListByParam($param);
        return ['list' => $list];
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 10:14
     */
    public function getAreaList($data = array())
    {
        $regionModel = new RegionModel();
        $param = [];
        $param['type'] = 2;
        $param['pid'] = $data['city_id'];
        $list = $regionModel->getListByParam($param);
        return ['list' => $list];
    }
}
