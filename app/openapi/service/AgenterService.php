<?php
namespace app\openapi\service;

use app\comm\model\AgenterGoodsSaleModel;
use app\comm\model\AgenterModel;
use app\comm\model\ClerkModel;
use app\comm\model\CompanyModel;
use app\comm\model\FeatureModel;
use app\comm\model\GoodsModel;
use app\comm\model\OrderModel;
use app\comm\model\ProductModel;
use app\comm\model\StoreGoodsSaleModel;
use app\comm\model\StoreModel;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class AgenterService extends BaseService
{
    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function getAgenterStoreList($data = array())
    {
        $commonService = new CommonService();
        $storeModel = new StoreModel();
        $agenterModel = new AgenterModel();
        $storeService = new StoreService();

        $clerk = $this->getClerkFromRedis($data['token']);
        if (!$clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone= $clerk['phone'];

        // 查询代理是否存在
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if (!$agenter) {
            apiFail('10001', '该手机号对应的代理已删除');
        }

        // 获取代理的医院列表
        $param = [];
        $param['agenter_id'] = $agenter['id'];
        $param['delete_time'] = 0;
        $param['check_status'] = 1;
        $storeList = $storeModel->getListByParam($param);
        $storeList = obj2arr($storeList);
        foreach ($storeList as $k => $v) {
            $storeList[$k]['province_name'] = $storeService->getRegionNameById($v['province_id']);
            $storeList[$k]['city_name'] = $storeService->getRegionNameById($v['city_id']);
            $storeList[$k]['area_name'] = $storeService->getRegionNameById($v['area_id']);
            $storeList[$k]['store_imgs'] = empty($v['store_imgs']) ? [] : $commonService->getImgFullUrlFormat(explode(',', $v['store_imgs']));
        }
        return ['list'=> $storeList];
    }

    /**
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/1 11:07
     */
    public function getStoreInfo($data = array())
    {
        $clerkModel = new ClerkModel();
        $storeModel = new StoreModel();
        $featureModel = new FeatureModel();
        $commonService = new CommonService();

        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $param = [];
        $param['id'] = $data['store_id'];
        $param['check_status'] = 1; // 审核通过
        $param['delete_time'] = 0;
        $store = $storeModel->getOneByParam($param);
        if (!$store) {
            apiFail('200004', '医院ID错误');
        }

        $ts = $store['ts'];
        $ts_name = '';
        if ($ts) {
            $ts_arr = explode('_', $ts);
            $ts_list = $featureModel->getListByParam([]);
            $ts_list = obj2arr($ts_list);
            $ts_id_name_list = array_column($ts_list, 'feature_name', 'id');
            $tmp = [];
            foreach ($ts_arr as $k => $v) {
                $tmp[] = isset($ts_id_name_list[$v]) ? $ts_id_name_list[$v] : '未知';
            }
            $ts_name = implode('_', $tmp);
        }
        $store['ts_name'] = $ts_name;
        $store['store_imgs'] = empty($store['store_imgs']) ? [] : $commonService->getImgFullUrlFormat(explode(',', $store['store_imgs']));

        // 获取当前门店的的助理和医生列表
        $param = [];
        $param['store_id'] = $data['store_id'];
        $param['position'] = ['in', [0, 1, 2]];
        $param['delete_time'] = 0;
        $clerkList = $clerkModel->getListByParam($param, null, null, null, ['position' => 'desc']);
        $clerkList = obj2arr($clerkList);
        foreach ($clerkList as $k => $v) {
            $clerkList[$k]['position_name'] = getPositionById($v['position']);
        }

        $result = [
            'store_info' => $store,
            'store_clerk_list' => $clerkList,
            'super_clerk_info' => empty($clerkList) ? [] : $clerkList[0]
        ];
        return $result;
    }

    /**
     * 获取通知列表接口
     * @Author Zed
     * @Time 2019/3/7 15:42
     */
    public function getNoticeList($data = [])
    {
        // 定义对象
        $orderModel = new OrderModel();
        $storeModel = new StoreModel();
        $clerkModel = new ClerkModel();
        $agenterModel = new AgenterModel();
        $storeService = new StoreService();
        $commonSrvice = new CommonService();
        $productModel = new ProductModel();

        // 验证用户
        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone = $cache_clerk['phone'];

        // 查询代理是否存在
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if (!$agenter) {
            apiFail('10001', '该手机号对应的代理已删除');
        }

        // 获取预订单列表
        $param = [];
        $param['agenter_id'] = $agenter['id']; // 审核通过
        $orderList = $orderModel->getListByParam($param, null, null, null, ['finish' => 'asc' ,'created_at' => 'desc']);
        $orderList = obj2arr($orderList);

        // 医院信息
        $store_ids = array_column($orderList, 'store_id');
        $param = [];
        $param['id'] = ['in', $store_ids];
        $storeList = $storeModel->getListByParam($param);
        $storeTmpList = [];
        foreach ($storeList as $k => $v) {
            $v['province_name'] = $storeService->getRegionNameById($v['province_id']);
            $v['city_name'] = $storeService->getRegionNameById($v['city_id']);
            $v['area_name'] = $storeService->getRegionNameById($v['area_id']);
            $storeTmpList[$v['id']] = $v;
        }

        // 医生信息
        $clerk_ids = array_column($orderList, 'clerk_id');
        $param = [];
        $param['id'] = ['in', $clerk_ids];
        $clerkList = $clerkModel->getListByParam($param);
        $clerkTmpList = [];
        foreach ($clerkList as $k => $v) {
            $clerkTmpList[$v['id']] = $v;
        }

        // 产品
        $product_ids = array_column($orderList, 'product_id');
        $param = [];
        $param['delete_time'] = 0;
        $param['id'] = ['in', $product_ids];
        $productList = $productModel->getListByParam($param);
        $producTmpList = [];
        foreach ($productList as $k => $v) {
            $v['img_url'] = $commonSrvice->getImgFullUrl(explode(',' ,$v['img_url']));
            $producTmpList[$v['id']] = $v;
        }

        foreach ($orderList as $k => $v) {
            $orderList[$k]['product_img_url'] =isset($producTmpList[$v['product_id']]) ? $producTmpList[$v['product_id']]['img_url'] : '';
            $orderList[$k]['clerkInfo'] = isset($clerkTmpList[$v['clerk_id']]) ? $clerkTmpList[$v['clerk_id']] : [];
            $orderList[$k]['store_name'] = isset($storeTmpList[$v['store_id']]) ? $storeTmpList[$v['store_id']]['store_name'] : '';
            $orderList[$k]['province_name'] =isset($storeTmpList[$v['store_id']]) ? $storeTmpList[$v['store_id']]['province_name'] : '';
            $orderList[$k]['city_name'] =isset($storeTmpList[$v['store_id']]) ? $storeTmpList[$v['store_id']]['city_name'] : '';
            $orderList[$k]['area_name'] = isset($storeTmpList[$v['store_id']]) ? $storeTmpList[$v['store_id']]['area_name'] : '';
            $orderList[$k]['address_detail'] = $orderList[$k]['province_name'] . $orderList[$k]['city_name'] . $orderList[$k]['area_name'] . isset($storeTmpList[$v['store_id']]) ? $storeTmpList[$v['store_id']]['address_detail'] : '';
            $orderList[$k]['created_at'] = date('Y-m-d H:i:s', $v['created_at']);
        }
        return ['list' => $orderList];
    }

    /**
     * 落实订单
     * @Author Zed
     * @Time 2019/3/7 15:42
     */
    public function FinishOrder($data = [])
    {
        $orderModel = new OrderModel();
        $agenterModel = new AgenterModel();

        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone = $cache_clerk['phone'];

        // 查询代理是否存在
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        if (!$agenter) {
            apiFail('10001', '该手机号对应的代理已删除');
        }
        $editData = [
            'finish' => 1
        ];

        $orderModel->updateById($editData, $data['order_id']);
    }

    /**
     * @param array $data
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/19 10:44
     */
    public function getAgenterInfo($data = [])
    {
        // 定义对象
        $agenterModel = new AgenterModel();
        $companyModel = new CompanyModel();
        $agenterGoodsModel = new AgenterGoodsSaleModel();

        // 验证token
        $cache_clerk = $this->getClerkFromRedis($data['token']);
        empty($cache_clerk) && apiFail('10003', 'token错误或已过期');
        $phone = $cache_clerk['phone'];

        // 获取代理信息
        $param = [];
        $param['phone'] = $phone;
        $param['delete_time'] = 0;
        $agenter = $agenterModel->getOneByParam($param);
        empty($agenter) && apiFail('10001', '该手机号对应的代理已删除');
        $agenter_id = $agenter['id'];

        // 获取公司信息
        $param = [];
        $param['id'] = $agenter['company_id'];
        $company = $companyModel->getOneByParam($param);
        empty($company) && apiFail('10001', '代理人的公司已被删除或不存在');

        // 获取代理销售数据
        $param = [];
        $agenterGoodsList = $agenterGoodsModel->getListByParam($param);
        $agenterGoodsList = obj2arr($agenterGoodsList);
        $tmp = [];
        foreach ($agenterGoodsList as $k => $v) {
            $tmp[$v['agenter_id']][] = $v['sale_num'];
        }
        $tmp2 = [];
        foreach ($tmp as $k2 => $v2) {
            $tmp2[$k2] = array_sum($v2);
        }
        arsort($tmp2);
        $keys = array_keys($tmp2);
        $rank = array_search($agenter_id, $keys);
        $percent = ((count($keys) - $rank -1) / count($keys)) * 100;
        if ($rank == 0) {
            $percent = 100;
        }

        // 返回信息
        $return = [
            'agenter_id' => $agenter_id,
            'phone' => $phone,
            'agenter_name' => $agenter['name'],
            'position' => '代理',
            'company_name' => $company['company_name'],
            'percent' => number_format($percent, 2)
        ];
        return $return;
    }

    /**
     * 落实订单
     * @Author Zed
     * @Time 2019/3/7 15:42
     */
    public function getDataList($data = [])
    {
        $goodsModel = new GoodsModel();
        $storeGoodsSaleModel = new StoreGoodsSaleModel();
        $agenterGoodsSaleModel = new AgenterGoodsSaleModel();
        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('10003', 'token错误或已过期');
        }
        $phone = $cache_clerk['phone'];

        // 获取商品信息
        $param = ['id' => $data['goods_id']];
        $goods = $goodsModel->getOneByParam($param);
        empty($goods) && apiFail('100004', '商品ID错误');

        // 条件处理
        if ($data['month_type'] == 1) {
            $start_time = $data['year'] . "-01-01 00:00:00";
            $end_time = $data['year'] . "-06-30 23:59:59";
        } else {
            $start_time = $data['year'] . "-07-01 00:00:00";
            $end_time = $data['year'] . "-12-31 23:59:59";
        }

        // 详细列表数据
        $param = ['goods_name_code' => $goods['goods_name_code'], 'created_at' => ['between', [$start_time, $end_time]], 'store_id' => $data['store_id']];
        $dataList = $data['role_type'] == 1 ? $agenterGoodsSaleModel->getListByParam($param) : $storeGoodsSaleModel->getListByParam($param);
        $dataList = obj2arr($dataList);

        $sixMonthData = [
            'month_1_buy' => 0,
            'month_2_buy'=> 0,
            'month_3_buy' => 0,
            'month_4_buy' => 0,
            'month_5_buy' => 0,
            'month_6_buy' => 0,
            'month_1_song' => 0,
            'month_2_song' => 0,
            'month_3_song' => 0,
            'month_4_song' => 0,
            'month_5_song' => 0,
            'month_6_song' => 0,
        ];
        $song = $data['role_type'] == 1 ? 'give_num' : 'song_num';
        foreach ($dataList as $k => $v) {
            if($data['month_type'] == 1) {
                if (strtotime($v['created_at']) >= strtotime($data['year']."-01-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-01-31 23:59:59")){
                    $sixMonthData['month_1_buy'] += $v['sale_num'];
                    $sixMonthData['month_1_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-02-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-02-29 23:59:59")){
                    $sixMonthData['month_2_buy'] += $v['sale_num'];
                    $sixMonthData['month_2_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-03-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-03-31 23:59:59")){
                    $sixMonthData['month_3_buy'] += $v['sale_num'];
                    $sixMonthData['month_3_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-04-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-04-30 23:59:59")){
                    $sixMonthData['month_4_buy'] += $v['sale_num'];
                    $sixMonthData['month_4_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-05-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-05-31 23:59:59")){
                    $sixMonthData['month_5_buy'] += $v['sale_num'];
                    $sixMonthData['month_5_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-06-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-06-30 23:59:59")){
                    $sixMonthData['month_6_buy'] += $v['sale_num'];
                    $sixMonthData['month_6_song'] += $v[$song];
                }
            } else {
                if (strtotime($v['created_at']) >= strtotime($data['year']."-07-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-07-31 23:59:59")){
                    $sixMonthData['month_1_buy'] += $v['sale_num'];
                    $sixMonthData['month_1_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-08-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-08-31 23:59:59")){
                    $sixMonthData['month_2_buy'] += $v['sale_num'];
                    $sixMonthData['month_2_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-09-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-09-30 23:59:59")){
                    $sixMonthData['month_3_buy'] += $v['sale_num'];
                    $sixMonthData['month_3_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-10-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-10-31 23:59:59")){
                    $sixMonthData['month_4_buy'] += $v['sale_num'];
                    $sixMonthData['month_4_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-11-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-11-30 23:59:59")){
                    $sixMonthData['month_5_buy'] += $v['sale_num'];
                    $sixMonthData['month_5_song'] += $v[$song];
                }
                if (strtotime($v['created_at']) >= strtotime($data['year']."-12-01 00:00:00") && strtotime($v['created_at']) <= strtotime($data['year']."-12-31 23:59:59")){
                    $sixMonthData['month_6_buy'] += $v['sale_num'];
                    $sixMonthData['month_6_song'] += $v[$song];
                }
            }
        }

        return ['dataList' => $dataList, 'monthData' => $sixMonthData];
    }
}
