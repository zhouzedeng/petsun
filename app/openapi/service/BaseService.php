<?php
namespace app\openapi\service;

use app\comm\model\RegionModel;
use think\Cache;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class BaseService
{

    /**
     * @param null $token
     * @return bool|mixed
     * @Author Zed
     * @Time 2019/3/1 11:00
     */
    public function getClerkFromRedis($token = null)
    {
        if (!$token) {
            return false;
        }
        $clerk = Cache::store('default')->get(getPetSunUserPrefixKey() . $token);
        if (!$clerk) {
            return false;
        }
        return $clerk;
    }

    /**
     * @param null $name
     * @return int|mixed
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 10:33
     */
    public function getRegionIdByName($name = null, $pid = 0)
    {
        $regionModel = new RegionModel();
        $param = [];
        $param['name'] = ['like', $name];
        $param['pid'] = $pid;
        $region = $regionModel->getOneByParam($param);
        if (empty($region)) {
            return 0;
        }
        return empty($region) ? 0 : $region['id'];
    }
}
