<?php
namespace app\openapi\service;

use app\comm\model\ClerkModel;
use app\comm\model\OrderModel;
use app\comm\model\ProductModel;
use app\comm\model\StoreModel;

/**
 * Class UserService
 * @package app\openapi\service
 * @Author Zed
 * @Time 2019/2/28 11:54
 */
class OrderService extends BaseService
{
    private $model = null;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->model = new OrderModel();
    }

    /**
     * 下单
     * @param array $data
     * @return array
     * @throws \think\exception
     * @Author Zed
     * @Time 2019/3/4 15:16
     */
    public function placeOrder($data = array())
    {
        $productModel = new ProductModel();
        $storeModel = new StoreModel();
        $clerkModel = new ClerkModel();

        $cache_clerk = $this->getClerkFromRedis($data['token']);
        if (!$cache_clerk) {
            apiFail('100003', 'token错误或已过期');
        }
        $phone = $cache_clerk['phone'];

        // 获取产品信息
        $param = [];
        $param['id'] = $data['product_id'];
        $param['delete_time'] = 0;
        $product = $productModel->getOneByParam($param);
        if (!$product) {
            apiFail('400002', '产品ID错误，没有找到该产品');
        }

        // 获取门店信息
        $param = [];
        $param['id'] = $data['store_id'];
        $param['delete_time'] = 0;
        $store = $storeModel->getOneByParam($param);
        if (!$store) {
            apiFail('400003', '医院ID错误，没有找到该医院');
        }
        if ($store['check_status'] != 1) {
            apiFail('400004', '该医院审核状态异常，不是审核通过状态');
        }

        // 获取当前登录者ID
        $param = [];
        $param['phone'] = $phone;
        $param['store_id'] = $data['store_id'];
        $param['delete_time'] = 0;
        $clerkInfo = $clerkModel->getOneByParam($param);
        if (!$clerkInfo) {
            apiFail('400005', '找不到店员信息');
        }

        $addData = [
            'product_id' => $product['id'],
            'product_name' => $product['name'],
            'num' => $data['number'],
            'unit_price' => $product['price'],
            'total_price' => $product['price'] * $data['number'],
            'clerk_id' => $clerkInfo['id'],
            'store_id' => $data['store_id'],
            'agenter_id' => $store['agenter_id'],
            'company_id' => $store['company_id'],
            'created_at' => time(),
        ];
        $this->model->add($addData);
    }
}
